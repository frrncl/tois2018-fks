%% compare_rq_analysis
% 
% Compares a reduced modal to a full model in order to assess the benefits
% of additional factors in an ANOVA model.

%% Synopsis
%
%  [] = compare_rq_analysis(trackID, splitID, rqRM, rqFM, balanced, sstype, quartile, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|rqRM|* - the identifier of ANOVA corresponding to the reduced model.
% * *|rqFM|* - the identifier of ANOVA corresponding to the full model.
% * *|balanced|* - the type of design to enforce. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros; 
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = compare_rq_analysis(trackID, splitID, rqRM, rqFM, balanced, sstype, quartile, startMeasure, endMeasure)

    % check the number of input parameters
    narginchk(7, 9);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
        
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
        
    
    % check that rqA is a non-empty cell array
    validateattributes(rqRM, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'rqA');
    
    if iscell(rqRM)
        % check that rqA is a cell array of strings with one element
        assert(iscellstr(rqRM) && numel(rqRM) == 1, ...
            'MATTERS:IllegalArgument', 'Expected rqA to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    rqRM = char(strtrim(rqRM));
    rqRM = rqRM(:).';
    
    % check that rqA assumes a valid value
    validatestring(rqRM, ...
        EXPERIMENT.analysis.list, '', 'rqA');
    

    % check that rqB is a non-empty cell array
    validateattributes(rqFM, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'rqB');
    
    if iscell(rqFM)
        % check that rqB is a cell array of strings with one element
        assert(iscellstr(rqFM) && numel(rqFM) == 1, ...
            'MATTERS:IllegalArgument', 'Expected rqB to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    rqFM = char(strtrim(rqFM));
    rqFM = rqFM(:).';
    
    % check that rqB assumes a valid value
    validatestring(rqFM, ...
        EXPERIMENT.analysis.list, '', 'rqB');    
    
    % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');
    
    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';
    
    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');
     
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
    % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');        
       
    if nargin == 9
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
    
    % start of overall computations
    startComputation = tic;
                  
    fprintf('\n\n######## Comparing %s to %s ANOVA analysis on track %s (%s) ########\n\n', ...
        rqRM, rqFM, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - reduced model:\n');
    fprintf('    * %s\n', rqRM);
    fprintf('    * %s\n', EXPERIMENT.analysis.(rqRM).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(rqRM).description);
    fprintf('  - full model:\n');
    fprintf('    * %s\n', rqRM);
    fprintf('    * %s\n', EXPERIMENT.analysis.(rqFM).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(rqFM).description);
    fprintf('  - analysis type:\n');
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));                
              
    % for each measure
    for m = startMeasure:endMeasure
         
        start = tic;
        
        mid = EXPERIMENT.measure.list{m};
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(rqRM, balanced, sstype, quartile, mid, splitID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(rqRM, balanced, sstype, quartile, mid, splitID, trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
            'WorkspaceVarNames', {'tblRM'}, ...
            'FileVarNames', {anovaTableID});
        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(rqFM, balanced, sstype, quartile, mid, splitID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(rqFM, balanced, sstype, quartile, mid, splitID, trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
            'WorkspaceVarNames', {'tblFM'}, ...
            'FileVarNames', {anovaTableID});
               
        clear anovaID anovaTableID 
        
        sseNum = tblRM{end-1, 2} - tblFM{end-1, 2};
        
        dfNum = tblRM{end-1, 3} - tblFM{end-1, 3};
        
        sseDen = tblFM{end-1, 2};
        
        dfDen = tblFM{end-1, 3};
        
        F = (sseNum ./ dfNum) ./ (sseDen ./ dfDen);
        
        p = 1 - fcdf(F, dfNum, dfDen);
        
        Fcrit = finv(1 - EXPERIMENT.analysis.alpha.threshold, dfNum, dfDen);
        
        fprintf('  - F = %10.4f\n', F);
        
        fprintf('  - p = %.4g\n', p);
        
        fprintf('  - Fcrit = %10.4f\n', Fcrit);
         
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
                     
    end % measure
                   
    fprintf('\n\n######## Total elapsed time for comparing %s to %s ANOVA analysis on track %s (%s): %s ########\n\n', ...
           rqRM, rqFM, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
