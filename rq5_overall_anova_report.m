%% rq5_overall_anova_report
% 
% Reports the overall summary of different RQ5 analyses for the given
% track and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = rq5_overall_anova_report(balanced, sstype, quartile, tau, startMeasure, endMeasure, varargin)
%  
% *Parameters*
%
% * *|balanced|* - the type of design to enforce. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros;
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|startmeasure|* - the index of the start measure to analyse. 
% * *|endmeasure|* - the index of the end measure to analyse. 
% * *|tau|* - a boolean indicating whether the Kendall's tau correlation
% has to be reported (|true|) or not (|false|).
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = rq5_overall_anova_report(balanced, sstype, quartile, tau, startMeasure, endMeasure, varargin)

    persistent TAG TAG0 TAG0_B TAG0_SST;
    
    if isempty(TAG)
        TAG = 'rq5';
        TAG0 = 'rq0';
        TAG0_B = 'b';
        TAG0_SST = 3;
    end
    
     % check the number of input parameters
    narginchk(6, inf);

    % load common parameters
    common_parameters

    % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');
        
    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';
    
    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');
     
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
    % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');
        
    % check that tau is a logical scalar
    validateattributes(tau, {'logical'}, ...
            {'nonempty', 'scalar'}, '', 'tau');
                           
    validateattributes(startMeasure, {'numeric'}, ...
        {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');

    validateattributes(endMeasure, {'numeric'}, ...
        {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        

    
    assert(mod(length(varargin), 2) == 0, ...
            'MATTERS:IllegalArgument', 'Expected an even number of (trackID, splitID) pairs in the variable length arguments.');
    
    trackID = {};
    splitID = {};
    
    for t = 1:2:length(varargin)
        
        % get the trackID
        tmpTrackID = varargin{t};
        
        % check that trackID is a non-empty string
        validateattributes(tmpTrackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

        if iscell(tmpTrackID)
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(tmpTrackID) && numel(tmpTrackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        tmpTrackID = char(strtrim(tmpTrackID));
        tmpTrackID = tmpTrackID(:).';
    
        % check that trackID assumes a valid value
        validatestring(tmpTrackID, ...
            EXPERIMENT.track.list, '', 'trackID');
              
        % get the splitID
        tmpSplitID = varargin{t+1};
        
        % check that splitID is a non-empty cell array
        validateattributes(tmpSplitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
        if iscell(tmpSplitID)
            % check that splitID is a cell array of strings with one element
            assert(iscellstr(tmpSplitID) && numel(tmpSplitID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
        end
    
        % remove useless white spaces, if any, and ensure it is a char row
        tmpSplitID = char(strtrim(tmpSplitID));
        tmpSplitID = tmpSplitID(:).';
    
        % check that splitID assumes a valid value
        validatestring(tmpSplitID, ...
            EXPERIMENT.split.list, '', 'splitID');
    
        % check that the track and the split rely on the same corpus
        assert(strcmp(EXPERIMENT.track.(tmpTrackID).corpus, EXPERIMENT.split.(tmpSplitID).corpus), 'Track %s and split %s do not rely on the same corpus', tmpTrackID, tmpSplitID);
        
        trackID = [trackID tmpTrackID];
        splitID = [splitID tmpSplitID];
                
    end
    
    tracks = length(trackID);
    
    % start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Reporting overall summary %s ANOVA analyses (%s) ########\n\n', ...
        TAG, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    
    fprintf('  - track(s) and split(s):\n');
    for t = 1:tracks        
        fprintf('    * track %s with split %s, shard(S) %d\n', trackID{t}, splitID{t}, EXPERIMENT.split.(splitID{t}).shard);
    end    
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));

        
    fprintf('+ Printing the report\n');
    
    % the file where the report has to be written
    reportID = EXPERIMENT.pattern.identifier.rep.anova.overall(TAG, balanced, sstype, quartile, splitID{1}, trackID{1});
    fid = fopen(EXPERIMENT.pattern.file.report('overall', reportID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a1paper,landscape]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
     fprintf(fid, '\\title{Overall Summary Report on %s ANOVA Analyses Across Different Tracks/Splits}\n\n', TAG);
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    
    fprintf(fid, 'Settings:\n');
    fprintf(fid, '\\begin{itemize}\n');    
    
    for t = 1:tracks
        fprintf(fid, '\\item track: %s -- %s; split: %s -- %s\n', ...
            strrep(trackID{t}, '_', '\_'), EXPERIMENT.track.(trackID{t}).name, ...
            strrep(splitID{t}, '_', '\_'), strrep(EXPERIMENT.split.(splitID{t}).name, '_', '\_'));
        fprintf(fid, '\\begin{itemize}\n');        
        fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID{t}).topics);
        fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID{t}).runs);        
        fprintf(fid, '\\item corpus: %s -- %s \n', strrep(EXPERIMENT.split.(splitID{t}).corpus, '_', '\_'), EXPERIMENT.corpus.(EXPERIMENT.split.(splitID{t}).corpus).name);
        fprintf(fid, '\\end{itemize}\n');
    end
        
    fprintf(fid, '\\item analysis type:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item %s: %s \n', TAG0, EXPERIMENT.analysis.(TAG0).name);
    fprintf(fid, '\\item model: %s \\\\ \n', EXPERIMENT.analysis.(TAG0).glmm);
    fprintf(fid, '%s\n', EXPERIMENT.analysis.(TAG0).description);
    fprintf(fid, '\\item balanced ANOVA: %s\n', char(TAG0_B*'true ' + (1-TAG0_B)*'false'));
    fprintf(fid, '\\item sum of squares type: %d\n', TAG0_SST);
    fprintf(fid, '\\item runs up to quartile: %d \\\\ \n', quartile);
    fprintf(fid, '(q1 is the upper quartile; q4 is the lower quartile)\n');
    fprintf(fid, '\\item significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf(fid, '\\end{itemize}\n');
    
    
    fprintf(fid, '\\item analysis type:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item %s: %s \n', TAG, EXPERIMENT.analysis.(TAG).name);
    fprintf(fid, '\\item model: %s \\\\ \n', EXPERIMENT.analysis.(TAG).glmm);
    fprintf(fid, '%s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf(fid, '\\item balanced ANOVA: %s\n', char(balanced*'true ' + (1-balanced)*'false'));
    fprintf(fid, '\\item sum of squares type: %d\n', sstype);
    fprintf(fid, '\\item runs up to quartile: %d \\\\ \n', quartile);
    fprintf(fid, '(q1 is the upper quartile; q4 is the lower quartile)\n');
    fprintf(fid, '\\item significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item analysed measures:\n');
    fprintf(fid, '\\begin{itemize}\n');
    for m = startMeasure:endMeasure        
        fprintf(fid, '\\item %s: %s\n', ...
            EXPERIMENT.measure.getAcronym(m), EXPERIMENT.measure.getName(m));        
    end
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\vspace*{1em}Rule of thumb for effect size $\\hat{\\omega}_{\\langle fact\\rangle}^2$:\n');
    fprintf(fid, '\\begin{itemize}\n');    
    fprintf(fid, '\\item large effect: $\\hat{\\omega}_{\\langle fact\\rangle}^2 \\geq 0.14$\n');
    fprintf(fid, '\\item medium effect: $0.06 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.14$\n');
    fprintf(fid, '\\item small effect: $0.01 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.06$\n');
    fprintf(fid, '\\item negative values have to be considered as $0$\n');
    fprintf(fid, '\\end{itemize}\n');
                
    fprintf(fid, '\\newpage\n');
  
    % for each track
    for t = 1:tracks
        
        corpusID = EXPERIMENT.track.(trackID{t}).corpus;
        
        pair = [trackID{t} '_' splitID{t}];
        
        % for each measure
        for m = startMeasure:endMeasure
            
            mid = EXPERIMENT.measure.list{m};
            
            try                
                % topic/system on whole corpus
                anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG0, TAG0_B, TAG0_SST, quartile, mid, corpusID, trackID{t});
                anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG0, TAG0_B, TAG0_SST, quartile, mid, corpusID, trackID{t});
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID{t}, anovaID), ...
                    'WorkspaceVarNames', {'me1'}, ...
                    'FileVarNames', {anovaMeID});
                
                
                % topic/system/shard
                anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, splitID{t}, trackID{t});
                anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, balanced, sstype, quartile, mid, splitID{t}, trackID{t});
                anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, balanced, sstype, quartile, mid, splitID{t}, trackID{t});
                anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG, balanced, sstype, quartile, mid, splitID{t}, trackID{t});
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID{t}, anovaID), ...
                    'WorkspaceVarNames', {'tbl5', 'soa5', 'me5'}, ...
                    'FileVarNames', {anovaTableID, anovaSoAID, anovaMeID});
                
            catch
                
                data.(pair).(mid).topic.p = NaN;
                data.(pair).(mid).topic.soa = NaN;
                
                data.(pair).(mid).system.p = NaN;
                data.(pair).(mid).system.soa = NaN;
                data.(pair).(mid).system.tau = NaN;
                
                data.(pair).(mid).shard.p = NaN;
                data.(pair).(mid).shard.soa = NaN;
                
                data.(pair).(mid).system_shard.p = NaN;
                data.(pair).(mid).system_shard.soa = NaN;
                
                data.(pair).(mid).topic_system.p = NaN;
                data.(pair).(mid).topic_system.soa = NaN;
                
                data.(pair).(mid).topic_shard.p = NaN;
                data.(pair).(mid).topic_shard.soa = NaN;
                
                
                continue;
            end
            
            % find how to order system on shards as systems on whole corpus
            [~, idx] = ismember(me1.factorA.label, me5.factorA.label);
            
            data.(pair).(mid).topic.p = tbl5{2, 7};
            data.(pair).(mid).topic.soa = soa5.omega2p.subject;
            
            data.(pair).(mid).system.p = tbl5{3, 7};
            data.(pair).(mid).system.soa = soa5.omega2p.factorA;
            data.(pair).(mid).system.tau = corr([me1.factorA.mean me5.factorA.mean(idx)], 'type', 'Kendall');
            data.(pair).(mid).system.tau = data.(pair).(mid).system.tau(2);
            
            data.(pair).(mid).shard.p = tbl5{4, 7};
            data.(pair).(mid).shard.soa = soa5.omega2p.factorB;
            
            data.(pair).(mid).system_shard.p = tbl5{5, 7};
            data.(pair).(mid).system_shard.soa = soa5.omega2p.factorA_factorB;
            
            data.(pair).(mid).topic_system.p = tbl5{6, 7};
            data.(pair).(mid).topic_system.soa = soa5.omega2p.subject_factorA;
            
            data.(pair).(mid).topic_shard.p = tbl5{7, 7};
            data.(pair).(mid).topic_shard.soa = soa5.omega2p.subject_factorB;
            
            clear me1 tbl5 soa5 me5;
        end    % measure
    end % track
        
            
    fprintf(fid, '\\begin{table}[p] \n');
    fprintf(fid, '\\centering \n');
    
    fprintf(fid, '\\caption{Effect size ($\\hat{\\omega}^2$ SoA) and p-value for model %s over different tracks and splits. Insignificant effects are in gray; small effects, light blue; medium, blue; and large, dark blue. The $\\tau$ reports system ranking correlation when using the whole corpus (RQ0) and shards (RQ5).}\n', ...
            EXPERIMENT.analysis.(TAG).glmm);
    
    fprintf(fid, '\\label{tab:smry-%s}\n', anovaID);
    
    fprintf(fid, '\\footnotesize \n');
    
    fprintf(fid, '\\hspace*{-6.5em} \n');
    
    if (tau)
        fprintf(fid, '\\begin{tabular}{|l|*{%d}{r|}} \n', 7*tracks);
    else
        fprintf(fid, '\\begin{tabular}{|l|*{%d}{r|}} \n', 6*tracks);
    end
    
    fprintf(fid, '\\hline\\hline \n');
    
    fprintf(fid, '\\textbf{Measure} ');
   
    for t = 1:tracks
        if (tau)
            fprintf(fid, '& \\multicolumn{7}{c|}{\\textbf{%s -- %s}} ', strrep(trackID{t}, '_', '\_'), strrep(EXPERIMENT.split.(splitID{t}).label, '_', '\_'));
        else
            fprintf(fid, '& \\multicolumn{6}{c|}{\\textbf{%s -- %s}} ', strrep(trackID{t}, '_', '\_'), strrep(EXPERIMENT.split.(splitID{t}).label, '_', '\_'));
        end
    end % track
    
    fprintf(fid, '\\\\ \n');
    
    if (tau)
        fprintf(fid, '\\cline{%d-%d}\n', 2, 7*tracks + 1);
    else
        fprintf(fid, '\\cline{%d-%d}\n', 2, 6*tracks + 1);
    end
    
    for t = 1:tracks
        
        if (tau)
            fprintf(fid, '& \\multicolumn{1}{c|}{$\\tau$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s*%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s*%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s*%s}\\rangle}$} ', ...
                EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB, ...
                EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB, ...
                EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, ...
                EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorB);
        else
            fprintf(fid, '& \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s*%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s*%s}\\rangle}$} & \\multicolumn{1}{c|}{$\\hat{\\omega}^2_{\\langle\\text{%s*%s}\\rangle}$} ', ...
                EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB, ...
                EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB, ...
                EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, ...
                EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorB);
        end
            
    end
    fprintf(fid, '\\\\ \n');
    
    fprintf(fid, '\\hline \n');
    
    for m = startMeasure:endMeasure
        mid = EXPERIMENT.measure.list{m};
        
        fprintf(fid, '%s ', EXPERIMENT.measure.(mid).acronym);
        
        for t = 1:tracks
                        
            pair = [trackID{t} '_' splitID{t}];
            
            % print the Kendall's tau correlation
            if (tau)
                fprintf(fid, ' & %5.4f ', data.(pair).(mid).system.tau);
            end
            
            
            % print the topic effect
            if data.(pair).(mid).topic.p <= EXPERIMENT.analysis.alpha.threshold
                if data.(pair).(mid).topic.soa < EXPERIMENT.analysis.smallEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.smallEffect.color, ...
                        data.(pair).(mid).topic.soa, ...
                        data.(pair).(mid).topic.p ...
                        );
                elseif data.(pair).(mid).topic.soa < EXPERIMENT.analysis.mediumEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.mediumEffect.color, ...
                        data.(pair).(mid).topic.soa, ...
                        data.(pair).(mid).topic.p);
                else
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.largeEffect.color, ...
                        data.(pair).(mid).topic.soa, ...
                        data.(pair).(mid).topic.p);
                end
            else
                fprintf(fid, ' & \\cellcolor{%s} -- (%3.2f) ', ...
                    EXPERIMENT.analysis.alpha.color, ...
                    data.(pair).(mid).topic.p);
            end
                        
            % print the system effect
            if data.(pair).(mid).system.p <= EXPERIMENT.analysis.alpha.threshold
                if data.(pair).(mid).system.soa < EXPERIMENT.analysis.smallEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.smallEffect.color, ...
                        data.(pair).(mid).system.soa, ...
                        data.(pair).(mid).system.p ...
                        );
                elseif data.(pair).(mid).system.soa < EXPERIMENT.analysis.mediumEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.mediumEffect.color, ...
                        data.(pair).(mid).system.soa, ...
                        data.(pair).(mid).system.p);
                else
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.largeEffect.color, ...
                        data.(pair).(mid).system.soa, ...
                        data.(pair).(mid).system.p);
                end
            else
                fprintf(fid, ' & \\cellcolor{%s} -- (%3.2f) ', ...
                    EXPERIMENT.analysis.alpha.color, ...
                    data.(pair).(mid).system.p);
            end
            
            % print the shard effect
            if data.(pair).(mid).shard.p <= EXPERIMENT.analysis.alpha.threshold
                if data.(pair).(mid).shard.soa < EXPERIMENT.analysis.smallEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.smallEffect.color, ...
                        data.(pair).(mid).shard.soa, ...
                        data.(pair).(mid).shard.p ...
                        );
                elseif data.(pair).(mid).shard.soa < EXPERIMENT.analysis.mediumEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.mediumEffect.color, ...
                        data.(pair).(mid).shard.soa, ...
                        data.(pair).(mid).shard.p);
                else
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.largeEffect.color, ...
                        data.(pair).(mid).shard.soa, ...
                        data.(pair).(mid).shard.p);
                end
            else
                fprintf(fid, ' & \\cellcolor{%s} -- (%3.2f) ', ...
                    EXPERIMENT.analysis.alpha.color, ...
                    data.(pair).(mid).shard.p);
            end
            
            % print the system*shard effect
            if data.(pair).(mid).system_shard.p <= EXPERIMENT.analysis.alpha.threshold
                if data.(pair).(mid).system_shard.soa < EXPERIMENT.analysis.smallEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.smallEffect.color, ...
                        data.(pair).(mid).system_shard.soa, ...
                        data.(pair).(mid).system_shard.p ...
                        );
                elseif data.(pair).(mid).system_shard.soa < EXPERIMENT.analysis.mediumEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.mediumEffect.color, ...
                        data.(pair).(mid).system_shard.soa, ...
                        data.(pair).(mid).system_shard.p);
                else
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.largeEffect.color, ...
                        data.(pair).(mid).system_shard.soa, ...
                        data.(pair).(mid).system_shard.p);
                end
            else
                fprintf(fid, ' & \\cellcolor{%s} -- (%3.2f) ', ...
                    EXPERIMENT.analysis.alpha.color, ...
                    data.(pair).(mid).system_shard.p);
            end
            
            % print the topic*system effect
            if data.(pair).(mid).topic_system.p <= EXPERIMENT.analysis.alpha.threshold
                if data.(pair).(mid).topic_system.soa < EXPERIMENT.analysis.smallEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.smallEffect.color, ...
                        data.(pair).(mid).topic_system.soa, ...
                        data.(pair).(mid).topic_system.p ...
                        );
                elseif data.(pair).(mid).topic_system.soa < EXPERIMENT.analysis.mediumEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.mediumEffect.color, ...
                        data.(pair).(mid).topic_system.soa, ...
                        data.(pair).(mid).topic_system.p);
                else
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.largeEffect.color, ...
                        data.(pair).(mid).topic_system.soa, ...
                        data.(pair).(mid).topic_system.p);
                end
            else
                fprintf(fid, ' & \\cellcolor{%s} -- (%3.2f) ', ...
                    EXPERIMENT.analysis.alpha.color, ...
                    data.(pair).(mid).topic_system.p);
            end
            
            % print the topic*shard effect
            if data.(pair).(mid).topic_shard.p <= EXPERIMENT.analysis.alpha.threshold
                if data.(pair).(mid).topic_shard.soa < EXPERIMENT.analysis.smallEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.smallEffect.color, ...
                        data.(pair).(mid).topic_shard.soa, ...
                        data.(pair).(mid).topic_shard.p ...
                        );
                elseif data.(pair).(mid).topic_shard.soa < EXPERIMENT.analysis.mediumEffect.threshold
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.mediumEffect.color, ...
                        data.(pair).(mid).topic_shard.soa, ...
                        data.(pair).(mid).topic_shard.p);
                else
                    fprintf(fid, ' & \\cellcolor{%s} %5.4f (%3.2f) ', ...
                        EXPERIMENT.analysis.largeEffect.color, ...
                        data.(pair).(mid).topic_shard.soa, ...
                        data.(pair).(mid).topic_shard.p);
                end
            else
                fprintf(fid, ' & \\cellcolor{%s} -- (%3.2f) ', ...
                    EXPERIMENT.analysis.alpha.color, ...
                    data.(pair).(mid).topic_shard.p);
            end
            
        end % track
                    
        fprintf(fid, '\\\\ \n');
        fprintf(fid, '\\hline \n');

    end % measure
            
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\end{table} \n\n');
        
    fprintf(fid, '\\end{document} \n\n');
        
    fclose(fid);
    
    
               
    fprintf('\n\n######## Total elapsed time for reporting %s ANOVA analysis across tracks and splits (%s): %s ########\n\n', ...
            TAG, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
