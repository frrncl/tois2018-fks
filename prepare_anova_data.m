%%  prepare_anova_data
% 
% Prepare measures for the subsequent ANOVA analyses and multiple
% comparison tests. 

% The data in the vector are sorted in descending order of marginal mean of 
% factorA, i.e. systems, in order to facilitate multiple comparison tests.
%
%% Synopsis
%
%   [N, R, T, S, data, subject, factorA, factorB] = prepare_anova_data(splitID, measures)
%  
% *Parameters*
%
% * *|splitID|* - the identifier of the split to process.
% * *|measures|* - a cell array of measure matrixes, one for each shard.
%
% *Returns*
%
% * *|N|* - the total number of elements in |data|.
% * *|T|* - the total number of topics.
% * *|R|* - the total number of runs.
% * *|S|* - the total number of shards.
% * *|data|* - the data vector, i.e. the |measures| properly laid out for
% ANOVA.
% * *|subject|* - the labels for the subjects, i.e. topics, in the |data|
% vector
% * *|factorA|* - the labels for the factorA, i.e. systems, in the |data|
% vector
% * *|factorB|* - the labels for the factorB, i.e. shards, in the |data|
% vector

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [N, R, T, S, data, subject, factorA, factorB] = prepare_anova_data(splitID, measures)

    % prepare the data in the ANOVA layout to understand how to order
    % systems
    [~, ~, ~, S, data, ~, factorA, ~] = layout_anova_data(splitID, measures);

    % order systems by descending mean of the measure average across
    % shards. This is the descending ordering which then will assumed
    % when reporting the multiple comparison tests.
    % Round to six digits after the decimal point to make sorting
    % stable
    [~, idx] = sort(round(grpstats(data(:), factorA(:), {'mean'}), 6), 'descend');

    % for each shard, reorder the systems
    for s = 1:S
        measures{s} = measures{s}(:, idx);
    end % for shard

    % prepare the data in the ANOVA layout for the actual analysis
    [N, R, T, S, data, subject, factorA, factorB] = layout_anova_data(splitID, measures);
end


function [N, R, T, S, data, subject, factorA, factorB] = layout_anova_data(splitID, measures)

    common_parameters;

    % the number of topics, runs, and shards
    T = height(measures{1});
    R = width(measures{1});
    S = EXPERIMENT.split.(splitID).shard;

    % total number of elements in the list
    N = T * R * S;

    % the data is layed out as follows:
    %
    % Data      Subject         FactorA FactorB
    % 0.10      T1              S1      SHARD1
    % 0.20      T2              S1      SHARD1
    % 0.30      T1              S2      SHARD1
    % 0.40      T2              S2      SHARD1
    % 0.50      T1              S3      SHARD1
    % 0.60      T2              S3      SHARD1
    % 0.15      T1              S1      SHARD2
    % 0.25      T2              S1      SHARD2
    % 0.35      T1              S2      SHARD2
    % 0.45      T2              S2      SHARD2
    % 0.55      T1              S3      SHARD2
    % 0.65      T2              S3      SHARD2

    data = NaN(1, N);
    shards = cell(1, S);
    
    % for each shard, 
    for s = 1:S
        %copy the measure on that shard in the correct range of the data
        data((s-1)*T*R + 1 : s*T*R) = measures{s}{:, :}(:);
        
        % add the identifier of the shard to the list of shards
        shards{s} = EXPERIMENT.pattern.identifier.shard(s);
    end
    
    % grouping variable for the subjects (topic)
    subject = repmat(measures{1}.Properties.RowNames, S*R, 1);

    % grouping variable for factorA (system)
    factorA = repmat(measures{1}.Properties.VariableNames, T, 1);
    factorA = repmat(factorA(:), S, 1);

    % grouping variable for factorB (shard)
    factorB = repmat(shards, T*R, 1);
    factorB = factorB(:);
end



