%% rq0_detailed_anova_report
% 
% Reports detailes about the different RQ0 analyses for the given
% track and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = rq0_detailed_anova_report(trackID, corpusID, sstype, quartile, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|corpusID|* - the identifier of the corpus to process.
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = rq0_detailed_anova_report(trackID, corpusID, sstype, quartile, startMeasure, endMeasure)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq0';
    end
    
    % check the number of input parameters
    narginchk(4, 6);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    % check that corpusID is a non-empty cell array
    validateattributes(corpusID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'corpusID');
    
    if iscell(corpusID)
        % check that corpusID is a cell array of strings with one element
        assert(iscellstr(corpusID) && numel(corpusID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected corpusID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    corpusID = char(strtrim(corpusID));
    corpusID = corpusID(:).';
    
    % check that splitID assumes a valid value
    validatestring(corpusID, ...
        EXPERIMENT.corpus.list, '', 'corpusID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, corpusID), 'Track %s and corpus %s do not rely on the same corpus', trackID, corpusID);
                  
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
    % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');
        
    if nargin == 6
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end    
     
    % start of overall computations
    startComputation = tic;
    
    % we always assume a balanced design on the whole corpus and check for
    % the absence of NaN!
    balanced = 'b';
    
    
    fprintf('\n\n######## Reporting detailed %s ANOVA analysis on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - corpus: %s\n', corpusID);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));

        
    fprintf('+ Printing the report\n');
    
    % the file where the report has to be written    
    reportID = EXPERIMENT.pattern.identifier.rep.anova.detailed(TAG, balanced, sstype, quartile, corpusID, trackID);
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, reportID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Detailed Report on %s ANOVA Analyses \\\\ on %s -- Corpus %s}\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, corpusID);
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    
    fprintf(fid, 'Settings:\n');
    fprintf(fid, '\\begin{itemize}\n');    

    fprintf(fid, '\\item track: %s -- %s \n', trackID, EXPERIMENT.track.(trackID).name);
    fprintf(fid, '\\begin{itemize}\n');        
    fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID).topics);
    fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID).runs);
    fprintf(fid, '\\end{itemize}\n');
    fprintf(fid, '\\item corpus: %s -- %s \n', strrep(corpusID, '_', '\_'), EXPERIMENT.corpus.(corpusID).name);
   
    fprintf(fid, '\\item analysis type:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item %s: %s \n', TAG, EXPERIMENT.analysis.(TAG).name);
    fprintf(fid, '\\item model: %s \\\\ \n', EXPERIMENT.analysis.(TAG).glmm);
    fprintf(fid, '%s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf(fid, '\\item balanced ANOVA: %s\n', char(balanced*'true ' + (1-balanced)*'false'));
    fprintf(fid, '\\item sum of squares type: %d\n', sstype);
    fprintf(fid, '\\item runs up to quartile: %d \\\\ \n', quartile);
    fprintf(fid, '(q1 is the upper quartile; q4 is the lower quartile)\n');
    fprintf(fid, '\\item significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf(fid, '\\end{itemize}\n');
        
    fprintf(fid, '\\item analysed measures:\n');
    fprintf(fid, '\\begin{itemize}\n');
    for m = startMeasure:endMeasure        
        fprintf(fid, '\\item %s: %s\n', ...
            EXPERIMENT.measure.getAcronym(m), EXPERIMENT.measure.getName(m));        
    end
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\vspace*{1em}Rule of thumb for effect size $\\hat{\\omega}_{\\langle fact\\rangle}^2$:\n');
    fprintf(fid, '\\begin{itemize}\n');    
    fprintf(fid, '\\item large effect: $\\hat{\\omega}_{\\langle fact\\rangle}^2 \\geq 0.14$\n');
    fprintf(fid, '\\item medium effect: $0.06 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.14$\n');
    fprintf(fid, '\\item small effect: $0.01 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.06$\n');
    fprintf(fid, '\\item negative values have to be considered as $0$\n');
    fprintf(fid, '\\end{itemize}\n');
    
        
    fprintf(fid, '\\newpage\n');
  
    % for each measure
    for m = startMeasure:endMeasure
        
        mid = EXPERIMENT.measure.list{m};
        
        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'tbl', 'soa'}, ...
                'FileVarNames', {anovaTableID, anovaSoAID});
            

        fprintf(fid, '\\begin{table}[p] \n');
        % fprintf(fid, '\\tiny \n');
        fprintf(fid, '\\centering \n');
        %fprintf(fid, '\\hspace*{-6.5em} \n');
        
        fprintf(fid, '\\caption{Model %s for %s using whole \\texttt{%s} corpus on track \\texttt{%s}.}\n', ...
            EXPERIMENT.analysis.(TAG).glmm, EXPERIMENT.measure.getAcronym(m), corpusID, trackID);
            
        fprintf(fid, '\\label{tab:dtl-%s}\n', anovaID);

        fprintf(fid, '\\begin{tabular}{|l|r|r|r|r|r|r|} \n');
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\multicolumn{1}{|c}{\\textbf{Source}} & \\multicolumn{1}{|c}{\\textbf{SS}} & \\multicolumn{1}{|c}{\\textbf{DF}} & \\multicolumn{1}{|c}{\\textbf{MS}} & \\multicolumn{1}{|c}{\\textbf{F}} & \\multicolumn{1}{|c}{\\textbf{p-value}} & \\multicolumn{1}{|c|}{$\\hat{\\omega}_{\\langle fact\\rangle}^2$} \\\\ \n');
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{%s}					& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            EXPERIMENT.analysis.label.subject, tbl{2, 2}, tbl{2, 3}, tbl{2, 5}, tbl{2, 6}, tbl{2, 7}, soa.omega2p.subject);
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{%s}					& %10.4f			& %d				& %10.4f		& %10.4f	& %.4g		& %10.4f \\\\', ...
            EXPERIMENT.analysis.label.factorA, tbl{3, 2}, tbl{3, 3}, tbl{3, 5}, tbl{3, 6}, tbl{3, 7}, soa.omega2p.factorA);
        
        fprintf(fid, '\\hline \n');
                
        fprintf(fid, '\\textbf{Error}                   & %10.4f			& %d				& %10.4f		&       	&      		&  \\\\', ...
            tbl{4, 2}, tbl{4, 3}, tbl{4, 5});
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\textbf{Total}                   & %10.4f			& %d				&       		&       	&      		&  \\\\', ...
            tbl{5, 2}, tbl{5, 3});
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{tabular} \n');

        fprintf(fid, '\\end{table} \n\n');
        
        
    end % for measure
    
    
    fprintf(fid, '\\end{document} \n\n');
    
    
    fclose(fid);
    
    fprintf('\n\n######## Total elapsed time for reporting detailed %s ANOVA analysis on track %s (%s): %s ########\n\n', ...
            TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
