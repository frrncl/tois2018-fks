%% rq5SIGIR17_analysis
% 
% Computes the Topic/System/Shard Effects ANOVA on the shards with
% system*shard, topic*system, and topic*shard interactions as in the 
% Ferro and Sanderson (SIGIR 2017) paper where only the system*shard
% interaction was considered.

%% Synopsis
%
%   [] = rq5SIGIR17_analysis(trackID, splitID, quartile, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq5SIGIR17_analysis(trackID, splitID, quartile, startMeasure, endMeasure)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq5SIGIR17'; 
    end

    % check the number of input parameters
    narginchk(3, 5);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
        
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
       
    % we always assume a balanced design with default sum of squares
    sstype = 3;

    % We obtain a balanced design by keeping only the topics which contain
    % at least one relevant document for each shard
    balanced = 'top';
    
    % the type of balancing performed
    blc.type = balanced;
    
    % We will report here the number of topics which adhere the above
    % constraint
    blc.value = NaN;      
        
     % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');   
       
    if nargin == 5
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
    
    % start of overall computations
    startComputation = tic;
                  
    fprintf('\n\n######## Performing %s ANOVA analysis on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));            
            
              
    % for each measure
    for m = startMeasure:endMeasure
         
        start = tic;
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        fprintf('  - loading the data\n');
        
        mid = EXPERIMENT.measure.list{m};
                        
        % load the whole corpus measure. The goal is to select the same Qx
        % systems used in rq1_analysis
        measureID = EXPERIMENT.pattern.identifier.measure.corpus(mid, EXPERIMENT.split.(splitID).corpus, trackID);
        
        serload2(EXPERIMENT.pattern.file.measure.corpus(trackID, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
        
        % determine the indexes of the systems in the requested quartile of
        % the measure with respect to the whole corpus
        idx = compute_quartile(quartile, measure);
        
        clear measure;
        
        measures = cell(1, EXPERIMENT.split.(splitID).shard);
        
        % for each shard, load the shard measures for the selected Qx
        % systems
        for s = 1:EXPERIMENT.split.(splitID).shard
            
            shardID = EXPERIMENT.pattern.identifier.shard(s);
            
            measureID = EXPERIMENT.pattern.identifier.measure.shard(mid, splitID, shardID, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure.shard(trackID, splitID, measureID), ...
                'WorkspaceVarNames', {'measure'}, ...
                'FileVarNames', {measureID});
            
            % just for AP
            if m == 1
                % determine the topics for which there are no relevant
                % documents
                topicsToSkip(:, s) = sum(isnan(measure{:, :}), 2) > 0;
            end
            
            % keep only systems in the requested quartile 
            measure = measure(:, idx);
            
            measures{s} = measure;
                        
            clear tmp measure;
            
        end % for shard
                                
        if m == 1
            fprintf('    * total number of topics without relevant documents across all the sub-corpora %d\n', ...
                sum(sum(topicsToSkip, 2) > 0));
            
            topicsToKeep = sum(topicsToSkip, 2) > 0;            
            topicsToKeep = ~topicsToKeep;
            
            % overload blc.value to report the total number of topics which
            % have at least one relavant document for each shard.
            % Bad but practical
            blc.value = sum(topicsToKeep);
        end
        
        
        % for each shard, remove the topics without relevant
        % documents across all the shards
        for s = 1:EXPERIMENT.split.(splitID).shard
            measures{s} = measures{s}(topicsToKeep, :);
                        
            % assume a balanced design, i.e. no NaN
            assert(~any(any(isnan(measures{s}{:, :}))), ...
                'MATTERS:IllegalState', 'A measure is expected to not contain any NaN value when computed on the whole corpus.');
            
        end
                
        % prepare the data for the ANOVA
        [N, R, T, ~, data, subject, factorA, factorB] = prepare_anova_data(splitID, measures);
                
        clear measures;
        
        fprintf('  - analysing the data\n');
                
        [~, tbl, sts] = EXPERIMENT.analysis.(TAG).compute(data, subject, ...
            factorA, factorB, sstype);
        
        df_subject = tbl{2,3};
        ss_subject = tbl{2,2};
        F_subject = tbl{2,6};
        
        df_factorA = tbl{3,3};
        ss_factorA = tbl{3,2};
        F_factorA = tbl{3,6};
        
        df_factorB = tbl{4,3};
        ss_factorB = tbl{4,2};
        F_factorB = tbl{4,6};
        
        df_factorA_factorB = tbl{5,3};
        ss_factorA_factorB = tbl{5,2};
        F_factorA_factorB = tbl{5,6};
        
        df_subject_factorA = tbl{6,3};
        ss_subject_factorA = tbl{6,2};
        F_subject_factorA = tbl{6,6};
        
        df_subject_factorB = tbl{7,3};
        ss_subject_factorB = tbl{7,2};
        F_subject_factorB = tbl{7,6};
       
        ss_error = tbl{8, 2};
        
        % compute the strength of association        
        soa.omega2p.subject = df_subject * (F_subject - 1) / (df_subject * (F_subject - 1) + N);
        soa.omega2p.factorA = df_factorA * (F_factorA - 1) / (df_factorA * (F_factorA - 1) + N);
        soa.omega2p.factorB = df_factorB * (F_factorB - 1) / (df_factorB * (F_factorB - 1) + N);
        soa.omega2p.factorA_factorB = df_factorA_factorB * (F_factorA_factorB - 1) / (df_factorA_factorB * (F_factorA_factorB - 1) + N);
        soa.omega2p.subject_factorA = df_subject_factorA * (F_subject_factorA - 1) / (df_subject_factorA * (F_subject_factorA - 1) + N);
        soa.omega2p.subject_factorB = df_subject_factorB * (F_subject_factorB - 1) / (df_subject_factorB * (F_subject_factorB - 1) + N);
        
        soa.eta2p.subject = ss_subject / (ss_subject + ss_error);
        soa.eta2p.factorA = ss_factorA / (ss_factorA + ss_error);
        soa.eta2p.factorB = ss_factorB / (ss_factorB + ss_error);
        soa.eta2p.factorA_factorB = ss_factorA_factorB / (ss_factorA_factorB + ss_error);
        soa.eta2p.subject_factorA = ss_subject_factorA / (ss_subject_factorA + ss_error);
        soa.eta2p.subject_factorB = ss_subject_factorB / (ss_subject_factorB + ss_error);

        % main effects
        me.subject.label = unique(subject, 'stable');
        [me.subject.mean, me.subject.ci] = grpstats(data(:), subject(:), {'mean', 'meanci'});
        
        me.factorA.label = unique(factorA, 'stable');
        [me.factorA.mean, me.factorA.ci] = grpstats(data(:), factorA(:), {'mean', 'meanci'});
        
        % Check that the marginal mean of factorA is actually sorted in
        % descending order, which is then assumed for analysing the
        % outcomes of the multiple comparison tests.
        % Round to six digits after the decimal point to make sorting
        % stable
        assert(issorted(round(me.factorA.mean, 6), 'descend'), ...
            'MATTERS:IllegalState', 'Expected main effect of factorA (%s) to be sorted in descending order.', EXPERIMENT.analysis.label.factorA);
        
        me.factorB.label = unique(factorB, 'stable');
        [me.factorB.mean, me.factorB.ci] = grpstats(data(:), factorB(:), {'mean', 'meanci'});
                
        % interaction between shards (x-axis) and systems (y-axis)
        % each row is a shard, columns are systems        
        ie.factorBA.factorA.label = me.factorA.label;
        ie.factorBA.factorB.label = me.factorB.label;
        ie.factorBA.mean = grpstats(data(:), {factorB(:),factorA(:)}, {'mean'});
        ie.factorBA.mean = reshape(ie.factorBA.mean, R, EXPERIMENT.split.(splitID).shard).';
                
        % interaction between systems (x-axis) and shards (y-axis)
        % each row is a system, columns are shards        
        ie.factorAB.factorA.label = me.factorA.label;
        ie.factorAB.factorB.label = me.factorB.label;
        ie.factorAB.mean = grpstats(data(:), {factorA(:),factorB(:)}, {'mean'});
        ie.factorAB.mean = reshape(ie.factorAB.mean, EXPERIMENT.split.(splitID).shard, R).';
        
        % interaction between topics (x-axis) and systems (y-axis)
        % each row is a topic, columns are systems        
        ie.subjectFactorA.subject.label = me.subject.label;
        ie.subjectFactorA.factorA.label = me.factorA.label;
        ie.subjectFactorA.mean = grpstats(data(:), {subject(:),factorA(:)}, {'mean'});
        ie.subjectFactorA.mean = reshape(ie.subjectFactorA.mean, R, T).';
               
        % interaction between system (x-axis) and topics (y-axis)
        % each row is a system, columns are topics        
        ie.factorASubject.subject.label = me.subject.label;
        ie.factorASubject.factorA.label = me.factorA.label;
        ie.factorASubject.mean = grpstats(data(:), {factorA(:),subject(:)}, {'mean'});
        ie.factorASubject.mean = reshape(ie.factorASubject.mean, T, R).';
        
        
        % interaction between topics (x-axis) and shards (y-axis)
        % each row is a topic, columns are shards        
        ie.subjectfactorB.subject.label = me.subject.label;
        ie.subjectfactorB.factorB.label = me.factorB.label;
        ie.subjectfactorB.mean = grpstats(data(:), {subject(:),factorB(:)}, {'mean'});
        ie.subjectfactorB.mean = reshape(ie.subjectfactorB.mean, EXPERIMENT.split.(splitID).shard, T).';
               
        % interaction between shards (x-axis) and topics (y-axis)
        % each row is a shard, columns are topics        
        ie.factorBSubject.subject.label = me.subject.label;
        ie.factorBSubject.factorB.label = me.factorB.label;
        ie.factorBSubject.mean = grpstats(data(:), {factorB(:),subject(:)}, {'mean'});
        ie.factorBSubject.mean = reshape(ie.factorBSubject.mean, T, EXPERIMENT.split.(splitID).shard).';
        
        % comparing systems
        mc = EXPERIMENT.analysis.multcompare.system(sts);
        
        % validate the model
        % normality of the residuals
        [~, vld.norm.p, vld.norm.stat, vld.norm.crit] = EXPERIMENT.analysis.validation.normality(sts);
        
        % homoscedasticity of the residuals for the topics
        [vld.homo.subject.p, vld.homo.subject.stats] = EXPERIMENT.analysis.validation.homoscedasticity(sts, subject);
        
        % homoscedasticity of the residuals for the systems
        [vld.homo.factorA.p, vld.homo.factorA.stats] = EXPERIMENT.analysis.validation.homoscedasticity(sts, factorA);
        
        % homoscedasticity of the residuals for the shards
        [vld.homo.factorB.p, vld.homo.factorB.stats] = EXPERIMENT.analysis.validation.homoscedasticity(sts, factorB);
        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaStatsID = EXPERIMENT.pattern.identifier.anova.sts(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaVldID = EXPERIMENT.pattern.identifier.anova.vld(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
            'WorkspaceVarNames', {'me', 'tbl', 'sts', 'blc', 'vld', 'soa', 'mc'}, ...
            'FileVarNames', {anovaMeID, anovaTableID, anovaStatsID, anovaBlcID, anovaVldID, anovaSoAID, anovaMcID});
        
        clear data me tbl sts blc vld soa mc;
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
                     
    end % measure
                   
    fprintf('\n\n######## Total elapsed time for performing %s ANOVA analysis on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end