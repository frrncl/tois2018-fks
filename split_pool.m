%% split_pool
% 
% Splits a pool into different pools, each one corresponding to one of the
% corpora of the specified split.
%
%% Synopsis
%
%   [] = split_pool(trackID, splitID, startShard, endShard)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|splitID|* - the identifier of the split to process.
% * *|startShard|* - the index of the start shard to process. Optional.
% * *|endShard|* - the index of the end shard to process. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = split_pool(trackID, splitID, startShard, endShard)
    
    % check that we have the correct number of input arguments. 
    narginchk(2, 4);
    
    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');    
    
    % check that splitID is a non-empty string
    validateattributes(splitID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');

    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');  
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
    
    if nargin == 4
        validateattributes(startShard, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.split.(splitID).shard }, '', 'startShard');
        
        validateattributes(endShard, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startShard, '<=', EXPERIMENT.split.(splitID).shard }, '', 'endShard');
    else 
        startShard = 1;
        endShard = EXPERIMENT.split.(splitID).shard;
    end    
        
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Splitting pool for track %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - corpus: %s\n', EXPERIMENT.split.(splitID).corpus);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    fprintf('  - slice \n');
    fprintf('    * start shard: %d\n', startShard);
    fprintf('    * end shard: %d\n', endShard);


    fprintf('+ Loading pool of the corpus\n');
        
    corpusPoolID = EXPERIMENT.pattern.identifier.pool.corpus(EXPERIMENT.split.(splitID).corpus, trackID);
    fprintf('  - %s\n', corpusPoolID);
    
    serload2(EXPERIMENT.pattern.file.dataset.corpus(trackID, corpusPoolID), ...
        'WorkspaceVarNames', {'corpusPool'}, ...
        'FileVarNames', {corpusPoolID});
    
    % the total number of topics in the pool
    T = height(corpusPool);
    

    % create a dummy table with a dummy document and the lowest relevance
    % degree possible among those in the pool    
    dummyTable = corpusPool{:, 1}{1, 1}(:, :);
    
    relevanceDegrees = categories(dummyTable.RelevanceDegree);

    idx = find(dummyTable.RelevanceDegree == relevanceDegrees(1), 1, 'first');
    
    dummyTable = dummyTable(idx, :);
    dummyTable(1, 'Document') = {'#####DUMMY_DOC_ID^^^^^^'};
    
    
    
    fprintf('+ Processing shards\n');
    
    % for each shard
    for s = startShard:endShard
    
        start = tic;
        
        shardID = EXPERIMENT.pattern.identifier.shard(s);
        
        fprintf('  - shard %s\n', shardID);
        
        fprintf('    * loading shard\n');
        
        shard = importdata(EXPERIMENT.pattern.file.shard(splitID, shardID));
        
        fprintf('    * computing shard pool\n');
                
        shardPool = corpusPool;
        
        % for each topic
        for t = 1:T
            
            % extract the inner table contained in the topic
            topic = shardPool{t, 1}{1, 1};

            % find which rows (documents) have to be kept
            keep = ismember(topic.Document, shard);

            if any(keep)
                shardPool{t, 1} = {topic(keep, :)};
            else
                shardPool{t, 1} = {dummyTable};
            end
            
        end % for topic
        
        
        shardPoolID = EXPERIMENT.pattern.identifier.pool.shard(splitID, shardID, trackID);
        shardPool.Properties.UserData.identifier = shardPoolID;
        shardPool.Properties.VariableNames = {shardPoolID};
                
        fprintf('    * saving shard pool\n');
        
        sersave2(EXPERIMENT.pattern.file.dataset.shard(trackID, splitID, shardPoolID), ...
            'WorkspaceVarNames', {'shardPool'}, ...
            'FileVarNames', {shardPoolID});
        
        clear shard shardPool
        
        fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end % for shard
    
    fprintf('\n\n######## Total elapsed time for splitting pool for track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
end

