%% rq5_overall_anova_counts_report
% 
% Reports the overall summary of different RQ5 analyses for the given
% track and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = rq5_overall_anova_counts_report(balanced, sstype, quartile, tau, startMeasure, endMeasure, varargin)
%  
% *Parameters*
%
% * *|balanced|* - the type of design to enforce. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros;
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|startmeasure|* - the index of the start measure to analyse. 
% * *|endmeasure|* - the index of the end measure to analyse. 
% * *|tau|* - a boolean indicating whether the Kendall's tau correlation
% has to be reported (|true|) or not (|false|).
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|varargin|* - even number of (trackID, splitID) pairs, whose analyses
% have to be summarized.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = rq5_overall_anova_counts_report(balanced, sstype, quartile, tau, startMeasure, endMeasure, varargin)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq5';
    end
    
     % check the number of input parameters
    narginchk(6, inf);

    % load common parameters
    common_parameters

    % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');
        
    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';
    
    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');     
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
    % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');
        
    % check that tau is a logical scalar
    validateattributes(tau, {'logical'}, ...
            {'nonempty', 'scalar'}, '', 'tau');
                           
    validateattributes(startMeasure, {'numeric'}, ...
        {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');

    validateattributes(endMeasure, {'numeric'}, ...
        {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        

    
    assert(mod(length(varargin), 2) == 0, ...
            'MATTERS:IllegalArgument', 'Expected an even number of (trackID, splitID) pairs in the variable length arguments.');
    
    trackID = {};
    splitID = {};
    
    for t = 1:2:length(varargin)
        
        % get the trackID
        tmpTrackID = varargin{t};
        
        % check that trackID is a non-empty string
        validateattributes(tmpTrackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

        if iscell(tmpTrackID)
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(tmpTrackID) && numel(tmpTrackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        tmpTrackID = char(strtrim(tmpTrackID));
        tmpTrackID = tmpTrackID(:).';
    
        % check that trackID assumes a valid value
        validatestring(tmpTrackID, ...
            EXPERIMENT.track.list, '', 'trackID');
              
        % get the splitID
        tmpSplitID = varargin{t+1};
        
        % check that splitID is a non-empty cell array
        validateattributes(tmpSplitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
        if iscell(tmpSplitID)
            % check that splitID is a cell array of strings with one element
            assert(iscellstr(tmpSplitID) && numel(tmpSplitID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
        end
    
        % remove useless white spaces, if any, and ensure it is a char row
        tmpSplitID = char(strtrim(tmpSplitID));
        tmpSplitID = tmpSplitID(:).';
    
        % check that splitID assumes a valid value
        validatestring(tmpSplitID, ...
            EXPERIMENT.split.list, '', 'splitID');
    
        % check that the track and the split rely on the same corpus
        assert(strcmp(EXPERIMENT.track.(tmpTrackID).corpus, EXPERIMENT.split.(tmpSplitID).corpus), 'Track %s and split %s do not rely on the same corpus', tmpTrackID, tmpSplitID);
        
        trackID = [trackID tmpTrackID];
        splitID = [splitID tmpSplitID];
                
    end
    
    tracks = length(trackID);
    
    % start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Reporting overall summary %s ANOVA analyses (%s) ########\n\n', ...
        TAG, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    
    fprintf('  - track(s) and split(s):\n');
    for t = 1:tracks        
        fprintf('    * track %s with split %s, shard(S) %d\n', trackID{t}, splitID{t}, EXPERIMENT.split.(splitID{t}).shard);
    end    
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));

        
    fprintf('+ Printing the report\n');
    
    % the file where the report has to be written
    reportID = EXPERIMENT.pattern.identifier.rep.anova.counts(TAG, balanced, sstype, quartile, splitID{1}, trackID{1});
    fid = fopen(EXPERIMENT.pattern.file.report('counts', reportID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a4paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
     fprintf(fid, '\\title{Overall Summary Counts Report on %s ANOVA Analyses Across Different Tracks/Splits}\n\n', TAG);
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    
    fprintf(fid, 'Settings:\n');
    fprintf(fid, '\\begin{itemize}\n');    
    
    for t = 1:tracks
        fprintf(fid, '\\item track: %s -- %s; split: %s -- %s\n', ...
            strrep(trackID{t}, '_', '\_'), EXPERIMENT.track.(trackID{t}).name, ...
            strrep(splitID{t}, '_', '\_'), strrep(EXPERIMENT.split.(splitID{t}).name, '_', '\_'));
        fprintf(fid, '\\begin{itemize}\n');        
        fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID{t}).topics);
        fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID{t}).runs);        
        fprintf(fid, '\\item corpus: %s -- %s \n', strrep(EXPERIMENT.split.(splitID{t}).corpus, '_', '\_'), EXPERIMENT.corpus.(EXPERIMENT.split.(splitID{t}).corpus).name);
        fprintf(fid, '\\end{itemize}\n');
    end
        
    
    fprintf(fid, '\\item analysis type:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item %s: %s \n', TAG, EXPERIMENT.analysis.(TAG).name);
    fprintf(fid, '\\item model: %s \\\\ \n', EXPERIMENT.analysis.(TAG).glmm);
    fprintf(fid, '%s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf(fid, '\\item balanced ANOVA: %s\n', balanced);
    fprintf(fid, '\\item sum of squares type: %d\n', sstype);
    fprintf(fid, '\\item runs up to quartile: %d \\\\ \n', quartile);
    fprintf(fid, '(q1 is the upper quartile; q4 is the lower quartile)\n');
    fprintf(fid, '\\item significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item analysed measures:\n');
    fprintf(fid, '\\begin{itemize}\n');
    for m = startMeasure:endMeasure        
        fprintf(fid, '\\item %s: %s\n', ...
            EXPERIMENT.measure.getAcronym(m), EXPERIMENT.measure.getName(m));        
    end
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\vspace*{1em}Rule of thumb for effect size $\\hat{\\omega}_{\\langle fact\\rangle}^2$:\n');
    fprintf(fid, '\\begin{itemize}\n');    
    fprintf(fid, '\\item large effect: $\\hat{\\omega}_{\\langle fact\\rangle}^2 \\geq 0.14$\n');
    fprintf(fid, '\\item medium effect: $0.06 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.14$\n');
    fprintf(fid, '\\item small effect: $0.01 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.06$\n');
    fprintf(fid, '\\item negative values have to be considered as $0$\n');
    fprintf(fid, '\\end{itemize}\n');
                
    fprintf(fid, '\\newpage\n');
    
    data.total.tracks = tracks;
    data.total.overall = tracks * (endMeasure - startMeasure + 1);
    
    
    data.count.overall.topic.notsig = 0;
    data.count.overall.topic.small = 0;
    data.count.overall.topic.medium = 0;
    data.count.overall.topic.large = 0;
    
    data.count.overall.system.notsig = 0;
    data.count.overall.system.small = 0;
    data.count.overall.system.medium = 0;
    data.count.overall.system.large = 0;
    
    data.count.overall.shard.notsig = 0;
    data.count.overall.shard.small = 0;
    data.count.overall.shard.medium = 0;
    data.count.overall.shard.large = 0;
    
    data.count.overall.system_shard.notsig = 0;
    data.count.overall.system_shard.small = 0;
    data.count.overall.system_shard.medium = 0;
    data.count.overall.system_shard.large = 0;
    
    data.count.overall.topic_system.notsig = 0;
    data.count.overall.topic_system.small = 0;
    data.count.overall.topic_system.medium = 0;
    data.count.overall.topic_system.large = 0;
    
    data.count.overall.topic_shard.notsig = 0;
    data.count.overall.topic_shard.small = 0;
    data.count.overall.topic_shard.medium = 0;
    data.count.overall.topic_shard.large = 0;
    
    % for each measure
    for m = startMeasure:endMeasure
        
        mid = EXPERIMENT.measure.list{m};
                
        data.count.(mid).topic.notsig = 0;
        data.count.(mid).topic.small = 0;
        data.count.(mid).topic.medium = 0;
        data.count.(mid).topic.large = 0;
        
        data.count.(mid).system.notsig = 0;
        data.count.(mid).system.small = 0;
        data.count.(mid).system.medium = 0;
        data.count.(mid).system.large = 0;
        
        data.count.(mid).shard.notsig = 0;
        data.count.(mid).shard.small = 0;
        data.count.(mid).shard.medium = 0;
        data.count.(mid).shard.large = 0;
                
        data.count.(mid).system_shard.notsig = 0;
        data.count.(mid).system_shard.small = 0;
        data.count.(mid).system_shard.medium = 0;
        data.count.(mid).system_shard.large = 0;
                
        data.count.(mid).topic_system.notsig = 0;
        data.count.(mid).topic_system.small = 0;
        data.count.(mid).topic_system.medium = 0;
        data.count.(mid).topic_system.large = 0;
                
        data.count.(mid).topic_shard.notsig = 0;
        data.count.(mid).topic_shard.small = 0;
        data.count.(mid).topic_shard.medium = 0;
        data.count.(mid).topic_shard.large = 0;
        
        
        % for each track
        for t = 1:tracks
                                                
            corpusID = EXPERIMENT.track.(trackID{t}).corpus;
            
            pair = [trackID{t} '_' splitID{t}];
                        
            % topic/system/shard
            anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, splitID{t}, trackID{t});
            anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, balanced, sstype, quartile, mid, splitID{t}, trackID{t});
            anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, balanced, sstype, quartile, mid, splitID{t}, trackID{t});
            
            serload2(EXPERIMENT.pattern.file.analysis(trackID{t}, anovaID), ...
                'WorkspaceVarNames', {'tbl', 'soa'}, ...
                'FileVarNames', {anovaTableID, anovaSoAID});
            
            
            % counts for the topic effect
            if tbl{2, 7} <= EXPERIMENT.analysis.alpha.threshold
                if soa.omega2p.subject < EXPERIMENT.analysis.smallEffect.threshold
                    data.count.(mid).topic.small = data.count.(mid).topic.small + 1;
                    data.count.overall.topic.small = data.count.overall.topic.small + 1;
                elseif soa.omega2p.subject < EXPERIMENT.analysis.mediumEffect.threshold
                    data.count.(mid).topic.medium = data.count.(mid).topic.medium + 1;
                    data.count.overall.topic.medium = data.count.overall.topic.medium + 1;
                else
                    data.count.(mid).topic.large = data.count.(mid).topic.large + 1;
                    data.count.overall.topic.large = data.count.overall.topic.large + 1;
                end
            else
                data.count.(mid).topic.notsig = data.count.(mid).topic.notsig + 1;
                data.count.overall.topic.notsig = data.count.overall.topic.notsig + 1;
            end
            
            % counts for the system effect
            if tbl{3, 7} <= EXPERIMENT.analysis.alpha.threshold
                if soa.omega2p.factorA < EXPERIMENT.analysis.smallEffect.threshold
                    data.count.(mid).system.small = data.count.(mid).system.small + 1;
                    data.count.overall.system.small = data.count.overall.system.small + 1;
                elseif soa.omega2p.factorA < EXPERIMENT.analysis.mediumEffect.threshold
                    data.count.(mid).system.medium = data.count.(mid).system.medium + 1;
                    data.count.overall.system.medium = data.count.overall.system.medium + 1;
                else
                    data.count.(mid).system.large = data.count.(mid).system.large + 1;
                    data.count.overall.system.large = data.count.overall.system.large + 1;
                end
            else
                data.count.(mid).system.notsig = data.count.(mid).system.notsig + 1;
                data.count.overall.system.notsig = data.count.overall.system.notsig + 1;
            end
            
             % counts for the shard effect
            if tbl{4, 7} <= EXPERIMENT.analysis.alpha.threshold
                if soa.omega2p.factorB < EXPERIMENT.analysis.smallEffect.threshold
                    data.count.(mid).shard.small = data.count.(mid).shard.small + 1;
                    data.count.overall.shard.small = data.count.overall.shard.small + 1;
                elseif soa.omega2p.factorB < EXPERIMENT.analysis.mediumEffect.threshold
                    data.count.(mid).shard.medium = data.count.(mid).shard.medium + 1;
                    data.count.overall.shard.medium = data.count.overall.shard.medium + 1;
                else
                    data.count.(mid).shard.large = data.count.(mid).shard.large + 1;
                    data.count.overall.shard.large = data.count.overall.shard.large + 1;
                end
            else
                data.count.(mid).shard.notsig = data.count.(mid).shard.notsig + 1;
                data.count.overall.shard.notsig = data.count.overall.shard.notsig + 1;
            end
            
             % counts for the system_shard effect
            if tbl{5, 7} <= EXPERIMENT.analysis.alpha.threshold
                if soa.omega2p.factorA_factorB < EXPERIMENT.analysis.smallEffect.threshold
                    data.count.(mid).system_shard.small = data.count.(mid).system_shard.small + 1;
                    data.count.overall.system_shard.small = data.count.overall.system_shard.small + 1;
                elseif soa.omega2p.factorA_factorB < EXPERIMENT.analysis.mediumEffect.threshold
                    data.count.(mid).system_shard.medium = data.count.(mid).system_shard.medium + 1;
                    data.count.overall.system_shard.medium = data.count.overall.system_shard.medium + 1;
                else
                    data.count.(mid).system_shard.large = data.count.(mid).system_shard.large + 1;
                    data.count.overall.system_shard.large = data.count.overall.system_shard.large + 1;
                end
            else
                data.count.(mid).system_shard.notsig = data.count.(mid).system_shard.notsig + 1;
                data.count.overall.system_shard.notsig = data.count.overall.system_shard.notsig + 1;
            end
            
             % counts for the topic_system effect
            if tbl{6, 7} <= EXPERIMENT.analysis.alpha.threshold
                if soa.omega2p.subject_factorA < EXPERIMENT.analysis.smallEffect.threshold
                    data.count.(mid).topic_system.small = data.count.(mid).topic_system.small + 1;
                    data.count.overall.topic_system.small = data.count.overall.topic_system.small + 1;
                elseif soa.omega2p.subject_factorA < EXPERIMENT.analysis.mediumEffect.threshold
                    data.count.(mid).topic_system.medium = data.count.(mid).topic_system.medium + 1;
                    data.count.overall.topic_system.medium = data.count.overall.topic_system.medium + 1;
                else
                    data.count.(mid).topic_system.large = data.count.(mid).topic_system.large + 1;
                    data.count.overall.topic_system.large = data.count.overall.topic_system.large + 1;
                end
            else
                data.count.(mid).topic_system.notsig = data.count.(mid).topic_system.notsig + 1;
                data.count.overall.topic_system.notsig = data.count.overall.topic_system.notsig + 1;
            end
            
             % counts for the topic_shard effect
            if tbl{7, 7} <= EXPERIMENT.analysis.alpha.threshold
                if soa.omega2p.subject_factorB < EXPERIMENT.analysis.smallEffect.threshold
                    data.count.(mid).topic_shard.small = data.count.(mid).topic_shard.small + 1;
                    data.count.overall.topic_shard.small = data.count.overall.topic_shard.small + 1;
                elseif soa.omega2p.subject_factorB < EXPERIMENT.analysis.mediumEffect.threshold
                    data.count.(mid).topic_shard.medium = data.count.(mid).topic_shard.medium + 1;
                    data.count.overall.topic_shard.medium = data.count.overall.topic_shard.medium + 1;
                else
                    data.count.(mid).topic_shard.large = data.count.(mid).topic_shard.large + 1;
                    data.count.overall.topic_shard.large = data.count.overall.topic_shard.large + 1;
                end
            else
                data.count.(mid).topic_shard.notsig = data.count.(mid).topic_shard.notsig + 1;
                data.count.overall.topic_shard.notsig = data.count.overall.topic_shard.notsig + 1;
            end
            
            clear tbl soa;
        end    % measure
    end % track
        
            
    fprintf(fid, '\\begin{table}[p] \n');
    fprintf(fid, '\\centering \n');
    
    fprintf(fid, '\\caption{Overall counts across measures and tracks for model %s. The table reports the number of times a factor is not significant or small/medium/large size; within parentheses there is the ratio.}\n', ...
        EXPERIMENT.analysis.(TAG).glmm);
    
    fprintf(fid, '\\label{tab:overall-counts-%s}\n', anovaID);
    
    fprintf(fid, '\\small \n');
    
    fprintf(fid, '\\begin{tabular}{|l|*{5}{r|}} \n');
    
    fprintf(fid, '\\hline\\hline \n');
    
    fprintf(fid, '\\multicolumn{1}{|c|}{\\textbf{Factor}} & \\multicolumn{1}{c|}{\\textbf{Not Significant}} & \\multicolumn{1}{c|}{\\textbf{Small Size}} & \\multicolumn{1}{c|}{\\textbf{Medium Size}} & \\multicolumn{1}{c|}{\\textbf{Large Size}} & \\multicolumn{1}{c|}{\\textbf{Total}} \\\\ \n');
    
    fprintf(fid, '\\hline \n');
    
    % topic effect
    fprintf(fid, ' %s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
        EXPERIMENT.analysis.label.subject, ...
        data.count.overall.topic.notsig, data.count.overall.topic.notsig / data.total.overall * 100, ...
        data.count.overall.topic.small, data.count.overall.topic.small / data.total.overall * 100, ...
        data.count.overall.topic.medium, data.count.overall.topic.medium / data.total.overall * 100, ...
        data.count.overall.topic.large, data.count.overall.topic.large / data.total.overall * 100, ...
        data.total.overall, 100.00);
    
    
    fprintf(fid, '\\hline \n');
    
    % system effect
    fprintf(fid, ' %s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
        EXPERIMENT.analysis.label.factorA, ...
        data.count.overall.system.notsig, data.count.overall.system.notsig / data.total.overall * 100, ...
        data.count.overall.system.small, data.count.overall.system.small / data.total.overall * 100, ...
        data.count.overall.system.medium, data.count.overall.system.medium / data.total.overall * 100, ...
        data.count.overall.system.large, data.count.overall.system.large / data.total.overall * 100, ...
        data.total.overall, 100.00);
    
    fprintf(fid, '\\hline \n');
    
    % shard effect
    fprintf(fid, ' %s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
        EXPERIMENT.analysis.label.factorB, ...
        data.count.overall.shard.notsig, data.count.overall.shard.notsig / data.total.overall * 100, ...
        data.count.overall.shard.small, data.count.overall.shard.small / data.total.overall * 100, ...
        data.count.overall.shard.medium, data.count.overall.shard.medium / data.total.overall * 100, ...
        data.count.overall.shard.large, data.count.overall.shard.large / data.total.overall * 100, ...
        data.total.overall, 100.00);
    
    fprintf(fid, '\\hline \n');
    
    % system_shard effect
    fprintf(fid, ' %s*%s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
        EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB, ...
        data.count.overall.system_shard.notsig, data.count.overall.system_shard.notsig / data.total.overall * 100, ...
        data.count.overall.system_shard.small, data.count.overall.system_shard.small / data.total.overall * 100, ...
        data.count.overall.system_shard.medium, data.count.overall.system_shard.medium / data.total.overall * 100, ...
        data.count.overall.system_shard.large, data.count.overall.system_shard.large / data.total.overall * 100, ...
        data.total.overall, 100.00);
    
    fprintf(fid, '\\hline \n');
    
    % topic_system effect
    fprintf(fid, ' %s*%s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
        EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, ...
        data.count.overall.topic_system.notsig, data.count.overall.topic_system.notsig / data.total.overall * 100, ...
        data.count.overall.topic_system.small, data.count.overall.topic_system.small / data.total.overall * 100, ...
        data.count.overall.topic_system.medium, data.count.overall.topic_system.medium / data.total.overall * 100, ...
        data.count.overall.topic_system.large, data.count.overall.topic_system.large / data.total.overall * 100, ...
        data.total.overall, 100.00);
    
    fprintf(fid, '\\hline \n');
        
    % topic_shard effect
    fprintf(fid, ' %s*%s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%)  \\\\ \n ', ...
        EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorB, ...
        data.count.overall.topic_shard.notsig, data.count.overall.topic_shard.notsig / data.total.overall * 100, ...
        data.count.overall.topic_shard.small, data.count.overall.topic_shard.small / data.total.overall, ...
        data.count.overall.topic_shard.medium, data.count.overall.topic_shard.medium / data.total.overall * 100, ...
        data.count.overall.topic_shard.large, data.count.overall.topic_shard.large / data.total.overall * 100, ...
        data.total.overall, 100.00);
    
    fprintf(fid, '\\hline \n');
        
   
            
    
    fprintf(fid, '\\hline\\hline \n');
    
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\end{table} \n\n');
    
        
    for m = startMeasure:endMeasure
        mid = EXPERIMENT.measure.list{m};
        
        fprintf(fid, '\\begin{table}[p] \n');
        fprintf(fid, '\\centering \n');
        
        fprintf(fid, '\\caption{(mid) counts for %s across tracks for model %s. The table reports the number of times a factor is not significant or small/medium/large size; within parentheses there is the ratio.}\n', ...
            EXPERIMENT.measure.(mid).acronym, EXPERIMENT.analysis.(TAG).glmm);
        
        fprintf(fid, '\\label{tab:%s-counts-%s}\n', mid, anovaID);
        
        fprintf(fid, '\\small \n');
        
        fprintf(fid, '\\begin{tabular}{|l|*{5}{r|}} \n');
        
        fprintf(fid, '\\hline\\hline \n');
        
        fprintf(fid, '\\multicolumn{1}{|c|}{\\textbf{Factor}} & \\multicolumn{1}{c|}{\\textbf{Not Significant}} & \\multicolumn{1}{c|}{\\textbf{Small Size}} & \\multicolumn{1}{c|}{\\textbf{Medium Size}} & \\multicolumn{1}{c|}{\\textbf{Large Size}} & \\multicolumn{1}{c|}{\\textbf{Total}} \\\\ \n');
        
        fprintf(fid, '\\hline \n');
        
        % topic effect
        fprintf(fid, ' %s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
            EXPERIMENT.analysis.label.subject, ...
            data.count.(mid).topic.notsig, data.count.(mid).topic.notsig / data.total.tracks * 100, ...
            data.count.(mid).topic.small, data.count.(mid).topic.small / data.total.tracks * 100, ...
            data.count.(mid).topic.medium, data.count.(mid).topic.medium / data.total.tracks * 100, ...
            data.count.(mid).topic.large, data.count.(mid).topic.large / data.total.tracks * 100, ...
            data.total.tracks, 100.00);
        
        
        fprintf(fid, '\\hline \n');
        
        % system effect
        fprintf(fid, ' %s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
            EXPERIMENT.analysis.label.factorA, ...
            data.count.(mid).system.notsig, data.count.(mid).system.notsig / data.total.tracks * 100, ...
            data.count.(mid).system.small, data.count.(mid).system.small / data.total.tracks * 100, ...
            data.count.(mid).system.medium, data.count.(mid).system.medium / data.total.tracks * 100, ...
            data.count.(mid).system.large, data.count.(mid).system.large / data.total.tracks * 100, ...
            data.total.tracks, 100.00);
        
        fprintf(fid, '\\hline \n');
        
        % shard effect
        fprintf(fid, ' %s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
            EXPERIMENT.analysis.label.factorB, ...
            data.count.(mid).shard.notsig, data.count.(mid).shard.notsig / data.total.tracks * 100, ...
            data.count.(mid).shard.small, data.count.(mid).shard.small / data.total.tracks * 100, ...
            data.count.(mid).shard.medium, data.count.(mid).shard.medium / data.total.tracks * 100, ...
            data.count.(mid).shard.large, data.count.(mid).shard.large / data.total.tracks * 100, ...
            data.total.tracks, 100.00);
        
        fprintf(fid, '\\hline \n');
        
        % system_shard effect
        fprintf(fid, ' %s*%s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
            EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB, ...
            data.count.(mid).system_shard.notsig, data.count.(mid).system_shard.notsig / data.total.tracks * 100, ...
            data.count.(mid).system_shard.small, data.count.(mid).system_shard.small / data.total.tracks * 100, ...
            data.count.(mid).system_shard.medium, data.count.(mid).system_shard.medium / data.total.tracks * 100, ...
            data.count.(mid).system_shard.large, data.count.(mid).system_shard.large / data.total.tracks * 100, ...
            data.total.tracks, 100.00);
        
        fprintf(fid, '\\hline \n');
        
        % topic_system effect
        fprintf(fid, ' %s*%s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
            EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, ...
            data.count.(mid).topic_system.notsig, data.count.(mid).topic_system.notsig / data.total.tracks * 100, ...
            data.count.(mid).topic_system.small, data.count.(mid).topic_system.small / data.total.tracks * 100, ...
            data.count.(mid).topic_system.medium, data.count.(mid).topic_system.medium / data.total.tracks * 100, ...
            data.count.(mid).topic_system.large, data.count.(mid).topic_system.large / data.total.tracks * 100, ...
            data.total.tracks, 100.00);
        
        fprintf(fid, '\\hline \n');
                
        % topic_shard effect
        fprintf(fid, ' %s*%s & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) & %d (%5.2f\\%%) \\\\ \n ', ...
            EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorB, ...
            data.count.(mid).topic_shard.notsig, data.count.(mid).topic_shard.notsig / data.total.tracks * 100, ...
            data.count.(mid).topic_shard.small, data.count.(mid).topic_shard.small / data.total.tracks * 100, ...
            data.count.(mid).topic_shard.medium, data.count.(mid).topic_shard.medium / data.total.tracks * 100, ...
            data.count.(mid).topic_shard.large, data.count.(mid).topic_shard.large / data.total.tracks * 100, ...
            data.total.tracks, 100.00);
        
        fprintf(fid, '\\hline\\hline \n');
        
        fprintf(fid, '\\end{tabular} \n');
        
        fprintf(fid, '\\end{table} \n\n');
        
    end % measure

       
    fprintf(fid, '\\end{document} \n\n');
        
    fclose(fid);
    
    
               
    fprintf('\n\n######## Total elapsed time for reporting %s ANOVA analysis across tracks and splits (%s): %s ########\n\n', ...
            TAG, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
