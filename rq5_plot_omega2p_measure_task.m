%% rq5_plot_omega2p_measure_cutoff
% 
% Plots the omega2p for different effects at different measure cut-offs.

%% Synopsis
%
%   [] = rq5_plot_omega2p_measure_cutoff(trackID, splitID, balanced, sstype, quartile, measure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|balanced|* - the type of design to enforce. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros;
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|measure|* - |precision| for omega2p using precision at cut-off;
% |ndgc| for omega2p using nDCD at cut-off.

%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq5_plot_omega2p_measure_task(balanced, sstype, quartile, effect, tracks, splits, measures)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq5';             
    end

    % check the number of input parameters
    narginchk(7, 7);

    % load common parameters
    common_parameters

    % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');
        
    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';
    
    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');
     
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
    % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');
    
        
    % check that effect is a non-empty cell array
    validateattributes(effect, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'effect');
    
    if iscell(effect)
        % check that effect is a cell array of strings with one element
        assert(iscellstr(effect) && numel(effect) == 1, ...
            'MATTERS:IllegalArgument', 'Expected effect to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    effect = char(strtrim(effect));
    effect = effect(:).';
        
    effectList = {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB, ...
        sprintf('%s*%s', EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA), ...
        sprintf('%s*%s', EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorB), ...
        sprintf('%s*%s', EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB), ...
        };
    
    % check that effect assumes a valid value
    validatestring(effect, ...
        effectList, '', 'effect');
        
    % check that the track and the split lists have the same length
        assert(length(tracks) == length(splits), 'Track (%d) and split (%d) lists must have the same length.', length(tracks), length(splits));
        
    for t = 1:length(tracks)
        
        trackID = tracks{t};
        % check that trackID is a non-empty string
        validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(trackID)
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(trackID) && numel(trackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        trackID = char(strtrim(trackID));
        trackID = trackID(:).';
        
        % check that trackID assumes a valid value
        validatestring(trackID, ...
            EXPERIMENT.track.list, '', 'trackID');
        
        % check that balanced is a logical scalar
        validateattributes(balanced, {'logical'}, ...
            {'nonempty', 'scalar'}, '', 'balanced');
    end
    
    for s = 1:length(splits)
        splitID = splits{s};
        trackID = tracks{s};
        
        % check that splitID is a non-empty cell array
        validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
        
        if iscell(splitID)
            % check that splitID is a cell array of strings with one element
            assert(iscellstr(splitID) && numel(splitID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        splitID = char(strtrim(splitID));
        splitID = splitID(:).';
        
        % check that splitID assumes a valid value
        validatestring(splitID, ...
            EXPERIMENT.split.list, '', 'splitID');
        
        % check that the track and the split rely on the same corpus
        assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
    end
        
    
    for m = 1:length(measures)
        mid = measures{m};
        
        % check that mid is a non-empty cell array
        validateattributes(mid, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'mid');
        
        if iscell(mid)
            % check that mid is a cell array of strings with one element
            assert(iscellstr(mid) && numel(mid) == 1, ...
                'MATTERS:IllegalArgument', 'Expected mid to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        mid = char(strtrim(mid));
        mid = mid(:).';
        
        % check that mid assumes a valid value
        validatestring(mid, ...
            EXPERIMENT.measure.list, '', 'mid');
    end
    
             
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s omega2p for effect %s (%s) ########\n\n', ...
        TAG, effect, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - effect: %s\n', effect);
    fprintf('  - tracks and splits:\n');
    for s = 1:length(splits)
        splitID = splits{s};
        trackID = tracks{s};
        fprintf('    * track %s; split %s; shard(s) %d\n', trackID, splitID, EXPERIMENT.split.(splitID).shard);
    end
    fprintf('  - measures\n');
    for m = 1:length(measures)
        fprintf('    * %s\n', EXPERIMENT.measure.(measures{m}).acronym);
    end
    
    S = length(splits);
    M = length(measures);
    
    data = NaN(S, M);
    
    measureLabels = cell(1, M);
    trackLabels = cell(1, S);
    

    % for each (track, split)
    for s = 1:S
        
        trackID = tracks{s};
        splitID = splits{s};
        
        trackLabels{s} = sprintf('%s - %s', strrep(trackID, '_', '\_'), strrep(splitID, '_', '\_'));
        
        
        
        % for each measure
        for m = 1:M
            
            mid = measures{m};
            measureLabels{m} = EXPERIMENT.measure.(mid).acronym;
            
            try
                
                % topic/system/shard
                anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, splitID, trackID);
                anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, balanced, sstype, quartile, mid, splitID, trackID);
                anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, balanced, sstype, quartile, mid, splitID, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                    'WorkspaceVarNames', {'tbl5', 'soa5'}, ...
                    'FileVarNames', {anovaTableID, anovaSoAID});
                
            catch
                
                soa.topic = NaN;
                soa.system = NaN;
                soa.shard = NaN;
                soa.system_shard = NaN;
                soa.topic_system = NaN;
                soa.topic_shard
                
                continue;
            end
            
            
            if (tbl5{2, 7} >= EXPERIMENT.analysis.alpha.threshold)
                soa.topic = NaN;
            else
                soa.topic = soa5.omega2p.subject;
            end
            
            if (tbl5{3, 7} >= EXPERIMENT.analysis.alpha.threshold)
                soa.system = NaN;
            else
                soa.system = soa5.omega2p.factorA;
            end
            
            if (tbl5{4, 7} >= EXPERIMENT.analysis.alpha.threshold)
                soa.shard = NaN;
            else
                soa.shard = soa5.omega2p.factorB;
            end
            
            if (tbl5{5, 7} >= EXPERIMENT.analysis.alpha.threshold)
                soa.system_shard = NaN;
            else
                soa.system_shard = soa5.omega2p.factorA_factorB;
            end
            
            if (tbl5{6, 7} >= EXPERIMENT.analysis.alpha.threshold)
                soa.topic_system = NaN;
            else
                soa.topic_system = soa5.omega2p.subject_factorA;
            end
            
            if (tbl5{7, 7} >= EXPERIMENT.analysis.alpha.threshold)
                soa.topic_shard = NaN;
            else
                soa.topic_shard = soa5.omega2p.subject_factorB;
            end
            
            
            switch lower(strtrim(effect))
                % topic effect
                case lower(effectList{1})
                    data(s, m) = soa.topic;
                    effect = effectList{1};
                    cmap = 'spring';                    
                    
                    % system effect
                case lower(effectList{2})
                    data(s, m) = soa.system;
                    effect = effectList{2};
                    cmap = 'winter';
                    
                    % shard effect
                case lower(effectList{3})
                    data(s, m) = soa.shard;
                    effect = effectList{3};
                    cmap = 'summer';
                    
                    % topic*system effect
                case lower(effectList{4})
                    data(s, m) = soa.topic_system;
                    effect = effectList{4};
                    cmap = 'hot';
                    
                    % topic*shard effect
                case lower(effectList{5})
                    data(s, m) = soa.topic_shard;
                    effect = effectList{5};
                    cmap = 'cool';
                    
                    % system*shard effect
                case lower(effectList{6})
                    data(s, m) = soa.system_shard;
                    effect = effectList{6};
                    cmap = 'copper';
            end
            
            clear tbl5 soa5;
        end    % measure
                        
    end % split/track
    
    
    
    currentFigure = figure('Visible', 'off');
    
    xTick = 1:S;
    yTick = 1:M;
    
    [X,Y] = meshgrid(xTick,yTick);
    
    sh = surf(X, Y, data.'); 
    
    sh.FaceAlpha = 0.6;
        
    cmp = colormap(cmap);
    cmp = cmp(end:-1:1, :);
    colormap(cmp)
    
    
    
 %   view(-60, 28);            
        
    
    ax = gca;
   % ax.TickLabelInterpreter = 'latex';
   % ax.FontSize = 24;
    
    ax.XLabel.String = 'Track and Shard';
    ax.XTick = xTick;
    ax.XTickLabel = trackLabels;
    %ax.TickLabelInterpreter = 'latex';
     
        
    ax.YLabel.String = 'Measure';
    ax.YTick = yTick;
    ax.YTickLabel = measureLabels;
    %ax.YLabel.Interpreter = 'latex';
    
    ax.ZLabel.String = sprintf('%s Effect Size', effect);
    %ax.ZLim = [0 1];
    
    
    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [42 22];
    currentFigure.PaperPosition = [1 1 40 20];
       
    figureID = EXPERIMENT.pattern.identifier.anova.soaPlot(TAG, balanced, sstype, quartile, effect, splits{1}, tracks{1});
    
    print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));
    
    close(currentFigure)
    
    fprintf('\n\n######## Total elapsed time for plotting %s omega2p for effect %s (%s): %s ########\n\n', ...
           TAG, effect, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
end

