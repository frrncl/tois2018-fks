%% split_runs
% 
% Splits a set of runs into different sets of runs, each one corresponding 
% to one of the subcorpora of the specified split.
%
%% Synopsis
%
%   [] = split_runs(trackID, splitID, keep, startShard, endShard)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|splitID|* - the identifier of the split to process.
% * *|keep|* - if |true| only the documents in the split are kept,
% otherwise they are removed. Optional.
% * *|startShard|* - the index of the start shard to process. Optional.
% * *|endShard|* - the index of the end shard to process. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = split_runs(trackID, splitID, keep, startShard, endShard)

    persistent PROCESS_RUN;
    
    if isempty(PROCESS_RUN)        
        PROCESS_RUN = @processRun;
    end

    % check that we have the correct number of input arguments. 
    narginchk(2, 5);
    
    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');    
    
    % check that splitID is a non-empty string
    validateattributes(splitID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');

    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');        

    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
    
    if nargin >= 3
         % check that keep is a logical scalar
        validateattributes(keep, {'logical'}, ...
                {'nonempty', 'scalar'}, '', 'keep');
    else
        keep = true;
    end
    
    if nargin == 5
        validateattributes(startShard, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.split.(splitID).shard }, '', 'startShard');
        
        validateattributes(endShard, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startShard, '<=', EXPERIMENT.split.(splitID).shard }, '', 'endShard');
    else 
        startShard = 1;
        endShard = EXPERIMENT.split.(splitID).shard;
    end
        
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Splitting runs for track %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - corpus: %s\n', EXPERIMENT.split.(splitID).corpus);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    fprintf('  - keep: %s\n', char(keep*'true ' + (1-keep)*'false'));
    fprintf('  - slice \n');
    fprintf('    * start shard: %d\n', startShard);
    fprintf('    * end shard: %d\n', endShard);


    fprintf('+ Loading runs of the corpus\n');
        
    corpusRunID = EXPERIMENT.pattern.identifier.run.corpus(EXPERIMENT.split.(splitID).corpus, trackID);
    fprintf('  - %s\n', corpusRunID);
    
    serload2(EXPERIMENT.pattern.file.dataset.corpus(trackID, corpusRunID), ...
        'WorkspaceVarNames', {'corpusRun'}, ...
        'FileVarNames', {corpusRunID});
    
    % the total number of topics in the set
    T = height(corpusRun);
    
    % the total number of runs in the set
    R = width(corpusRun);
    
    % replicate keep to properly work with cellfun
    keep = repmat({keep}, 1, R);
   
    fprintf('+ Processing shards\n');
    
    % for each shard
    for s = startShard:endShard
    
        start = tic;
        
        shardID = EXPERIMENT.pattern.identifier.shard(s);
        
        fprintf('  - shard %s\n', shardID);
        
        fprintf('    * loading shard\n');
        
        shard = importdata(EXPERIMENT.pattern.file.shard(splitID, shardID));
        
        % improves subsequent lookup operations
        shard = sort(shard);
        
        % replicate the shard to properly work with cellfun
        shard = repmat({shard}, 1, R);
                
        fprintf('    * computing shard runs\n');
        
        shardRun = corpusRun;
        
        for t = 1:T            
            shardRun{t, :} = cellfun(PROCESS_RUN, shardRun{t, :}, shard, keep, 'UniformOutput', false);            
        end % for topic
                
        shardRunID = EXPERIMENT.pattern.identifier.run.shard(splitID, shardID, trackID);
        shardRun.Properties.UserData.identifier = shardRunID;
                
        fprintf('    * saving shard runs\n');
        
        sersave2(EXPERIMENT.pattern.file.dataset.shard(trackID, splitID, shardRunID), ...
            'WorkspaceVarNames', {'shardRun'}, ...
            'FileVarNames', {shardRunID});
        
        clear shard shardRun
        
        fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end % for shard
    
    fprintf('\n\n######## Total elapsed time for splitting runs for track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
end


function [shardTopic] = processRun(topic, shard, keep)

    persistent DUMMY_TABLE;
   
    if isempty(DUMMY_TABLE)        
        DUMMY_TABLE = cell2table({'#####DUMMY_DOC_ID^^^^^^', 1, 1});
        DUMMY_TABLE.Properties.VariableNames = {'Document', 'Rank', 'Score'};        
    end
    
    % find which rows (documents) have to be kept
    if keep
        docs = ismember(topic.Document, shard);
    else
        docs = ~ismember(topic.Document, shard);
    end
    
    % if there is at least one document, use it, otherwise use a default
    % dummy document, which will be not relevant when assessed.
    if any(docs)
        shardTopic = topic(docs, :);
    else
        shardTopic = DUMMY_TABLE;
    end
end
