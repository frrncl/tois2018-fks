%% plot_shard_pool_relevant
% 
% Plots the heatmap of the number of relevant documents in the topic by
% shard matrix.

%% Synopsis
%
%   [] = plot_shard_pool_relevant(trackID, splitID)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = plot_shard_pool_relevant(trackID, splitID)

    persistent TAG PREC NDCG LINE_WIDTH MARKER_SIZE;
    
    if isempty(TAG)
        TAG = 'rq5';      
        PREC = {'p5', 'p10', 'p20', 'p50', 'p100'};
        NDCG = {'ndcg5', 'ndcg10', 'ndcg20', 'ndcg50', 'ndcg100'};
        LINE_WIDTH = 3.5;
        MARKER_SIZE = 25;
        
    end

    % check the number of input parameters
    narginchk(2, 2);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
        
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
    
     
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting topic/shard heatmap of relevant documents on track %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));    
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);

    shardStatsID = EXPERIMENT.pattern.identifier.stats.shard(splitID, trackID);
    
    serload2(EXPERIMENT.pattern.file.analysis(trackID, shardStatsID), ...
            'WorkspaceVarNames', {'shardStats'}, ...
            'FileVarNames', {shardStatsID});
    
          
    currentFigure = figure('Visible', 'off');
        
    h = heatmap(shardStats.pool.data);
    h.XLabel = EXPERIMENT.analysis.label.factorB;
    h.YLabel = EXPERIMENT.analysis.label.subject;
    h.Title = sprintf('%s - %s', trackID, splitID);
    h.CellLabelFormat = '%d';
    h.ColorScaling = 'scaled'; 
    h.Colormap = autumn;
    h.Colormap = h.Colormap(end:-1:1, :);
    
    if (min(min(shardStats.pool.data)) == 0) 
        h.Colormap(1, :) = [1.0, 1.0, 1.0];
    end
        
    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [9+ceil(size(shardStats,2)*32.0) 32];
    currentFigure.PaperPosition = [1 1 7+ceil(size(shardStats,2)*32.0) 30];
    
    print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, shardStatsID));
    
    close(currentFigure)
    
    fprintf('\n\n######## Total elapsed time for plotting topic/shard heatmap of relevant documents on track %s (%s): %s ########\n\n', ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
end

