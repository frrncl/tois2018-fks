%% rq1_analysis
% 
% Computes the Topic/System Effects ANOVA on the shards.

%% Synopsis
%
%   [] = rq1_analysis(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|balanced|* - the type of design to enforce. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros; 
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq1_analysis(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq1';      
    end

    % check the number of input parameters
    narginchk(5, 7);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
        
      % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
            
    % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');
        
    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';
    
    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');
    
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
    % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');        
    
    if nargin == 7
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
    
    % start of overall computations
    startComputation = tic;
                  
    fprintf('\n\n######## Performing %s ANOVA analysis on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));            
                          
    % for each measure
    for m = startMeasure:endMeasure
         
        start = tic;
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        fprintf('  - loading the data\n');
        
        mid = EXPERIMENT.measure.list{m};
                        
        % load the whole corpus measure. The goal is to select the same Qx
        % systems used in rq1_analysis
        measureID = EXPERIMENT.pattern.identifier.measure.corpus(mid, EXPERIMENT.split.(splitID).corpus, trackID);
        
        serload2(EXPERIMENT.pattern.file.measure.corpus(trackID, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
        
        % determine the indexes of the systems in the requested quartile of
        % the measure with respect to the whole corpus
        idx = compute_quartile(quartile, measure);
        
        clear measure;
        
        measures = cell(1, EXPERIMENT.split.(splitID).shard);
        
        % for each shard, load the shard measures
        for s = 1:EXPERIMENT.split.(splitID).shard
            
            shardID = EXPERIMENT.pattern.identifier.shard(s);
            
            measureID = EXPERIMENT.pattern.identifier.measure.shard(mid, splitID, shardID, trackID);
            
            serload2(EXPERIMENT.pattern.file.measure.shard(trackID, splitID, measureID), ...
                'WorkspaceVarNames', {'measure'}, ...
                'FileVarNames', {measureID});
            
            measures{s} = measure;
            
            clear tmp measure;
            
        end % for shard
        
        % compute the needed balancing value and balance the measures 
        [blc, measures] = compute_balancing(splitID, measures, balanced, idx);
        fprintf('    * balanced: %s with %3.2f\n', blc.type, blc.value);
                        
        % prepare the data for the ANOVA
        [N, ~, ~, ~, data, subject, factorA, ~] = prepare_anova_data(splitID, measures);
                
        clear measures;
        
        fprintf('  - analysing the data\n');
                
        [~, tbl, sts] = EXPERIMENT.analysis.(TAG).compute(data, subject, factorA, sstype);
        
        df_subject = tbl{2,3};
        ss_subject = tbl{2,2};
        F_subject = tbl{2,6};
        
        df_factorA = tbl{3,3};
        ss_factorA = tbl{3,2};
        F_factorA = tbl{3,6};
                
        ss_error = tbl{4, 2};
        
        % compute the strength of association
        soa.omega2p.subject = df_subject * (F_subject - 1) / (df_subject * (F_subject - 1) + N);
        soa.omega2p.factorA = df_factorA * (F_factorA - 1) / (df_factorA * (F_factorA - 1) + N);
        
        soa.eta2p.subject = ss_subject / (ss_subject + ss_error);
        soa.eta2p.factorA = ss_factorA / (ss_factorA + ss_error);
        
        % main effects
        me.subject.label = unique(subject, 'stable');
        [me.subject.mean, me.subject.ci] = grpstats(data(:), subject(:), {'mean', 'meanci'});
        
        me.factorA.label = unique(factorA, 'stable');
        [me.factorA.mean, me.factorA.ci] = grpstats(data(:), factorA(:), {'mean', 'meanci'});
        
        % Check that the marginal mean of factorA is actually sorted in
        % descending order, which is then assumed for analysing the
        % outcomes of the multiple comparison tests.
        % Round to six digits after the decimal point to make sorting
        % stable
        assert(issorted(round(me.factorA.mean, 6), 'descend'), ...
            'MATTERS:IllegalState', 'Expected main effect of factorA (%s) to be sorted in descending order.', EXPERIMENT.analysis.label.factorA);
        
        % multple comparison of systems
        mc = EXPERIMENT.analysis.multcompare.system(sts);
        
        % validate the model
        % normality of the residuals
        [~, vld.norm.p, vld.norm.stat, vld.norm.crit] = EXPERIMENT.analysis.validation.normality(sts);    
        
        % homoscedasticity of the residuals for the topics
        [vld.homo.subject.p, vld.homo.subject.stats] = EXPERIMENT.analysis.validation.homoscedasticity(sts, subject);
        
        % homoscedasticity of the residuals for the systems
        [vld.homo.factorA.p, vld.homo.factorA.stats] = EXPERIMENT.analysis.validation.homoscedasticity(sts, factorA);
                        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaStatsID = EXPERIMENT.pattern.identifier.anova.sts(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaVldID = EXPERIMENT.pattern.identifier.anova.vld(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
            'WorkspaceVarNames', {'me', 'tbl', 'sts', 'blc', 'vld', 'soa', 'mc'}, ...
            'FileVarNames', {anovaMeID, anovaTableID, anovaStatsID, anovaBlcID, anovaVldID, anovaSoAID, anovaMcID});
        
        clear data me tbl sts blc vld soa mc;
                       
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
                     
    end % for measure
                   
    fprintf('\n\n######## Total elapsed time for performing %s ANOVA analysis on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
