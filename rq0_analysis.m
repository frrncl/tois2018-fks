%% rq0_analysis
% 
% Computes the Topic/System effects ANOVA on the whole corpus.

%% Synopsis
%
%   [] = rq0_analysis(trackID, corpusID, sstype, quartile, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|corpusID|* - the identifier of the corpus to process.
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq0_analysis(trackID, corpusID, sstype, quartile, startMeasure, endMeasure)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq0';      
    end

    % check the number of input parameters
    narginchk(4, 6);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
        
    % check that corpusID is a non-empty cell array
    validateattributes(corpusID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'corpusID');
    
    if iscell(corpusID)
        % check that corpusID is a cell array of strings with one element
        assert(iscellstr(corpusID) && numel(corpusID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected corpusID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    corpusID = char(strtrim(corpusID));
    corpusID = corpusID(:).';
    
    % check that splitID assumes a valid value
    validatestring(corpusID, ...
        EXPERIMENT.corpus.list, '', 'corpusID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, corpusID), 'Track %s and corpus %s do not rely on the same corpus', trackID, corpusID);
         
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
    % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');
    
    if nargin == 6
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
    
    % start of overall computations
    startComputation = tic;
    
    % we always assume a balanced design on the whole corpus and check for
    % the absence of NaN!
    balanced = 'b';
    
    % the type of balancing performed
    blc.type = balanced;
    % since we do not force balancing, because we assume it, the balancing
    % value is NaN
    blc.value = NaN;      
                  
    fprintf('\n\n######## Performing %s ANOVA analysis on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - corpus: %s\n', corpusID);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
            
              
    % for each measure
    for m = startMeasure:endMeasure
         
        start = tic;
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        fprintf('  - loading the data\n');
        
        mid = EXPERIMENT.measure.list{m};
                        
        % load the whole corpus measure
        measureID = EXPERIMENT.pattern.identifier.measure.corpus(mid, corpusID, trackID);
        
        serload2(EXPERIMENT.pattern.file.measure.corpus(trackID, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
        
        % determine the indexes of the systems in the requested quartile of
        % the measure with respect to the whole corpus
        idx = compute_quartile(quartile, measure);
        
        % reorder systems by descending mean of the measure on the
        % whole corpus and keep only those in the requested quartile
        measure = measure(:, idx);
        
        % the number of topics and runs
        T = height(measure);
        R = width(measure);
        
        % total number of elements in the list
        N = T * R;
        
        data = measure{:, :}(:);
        subject = repmat(measure.Properties.RowNames, R, 1);
        factorA = repmat(measure.Properties.VariableNames, T, 1);
        factorA = factorA(:);
        
        clear measure;
                
        fprintf('  - analysing the data\n');
                
        [~, tbl, sts] = EXPERIMENT.analysis.(TAG).compute(data, subject, ...
            factorA, sstype);
        
        df_subject = tbl{2,3};
        ss_subject = tbl{2,2};
        F_subject = tbl{2,6};
        
        df_factorA = tbl{3,3};
        ss_factorA = tbl{3,2};
        F_factorA = tbl{3,6};
                
        ss_error = tbl{4, 2};
        
        % compute the strength of association        
        soa.omega2p.subject = df_subject * (F_subject - 1) / (df_subject * (F_subject - 1) + N);
        soa.omega2p.factorA = df_factorA * (F_factorA - 1) / (df_factorA * (F_factorA - 1) + N);
        
        soa.eta2p.subject = ss_subject / (ss_subject + ss_error);
        soa.eta2p.factorA = ss_factorA / (ss_factorA + ss_error);

        % main effects
        me.subject.label = unique(subject, 'stable');
        [me.subject.mean, me.subject.ci] = grpstats(data(:), subject(:), {'mean', 'meanci'});
        
        me.factorA.label = unique(factorA, 'stable');
        [me.factorA.mean, me.factorA.ci] = grpstats(data(:), factorA(:), {'mean', 'meanci'});
               
        assert(issorted(me.factorA.mean, 'descend'), ...
            'MATTERS:IllegalState', 'Expected main effect of factorA (%s) to be sorted in descending order.', EXPERIMENT.analysis.label.factorA);
       
        % multiple comparison of systems
        mc = EXPERIMENT.analysis.multcompare.system(sts);
        
        % validate the model
        % normality of the residuals
        [~, vld.norm.p, vld.norm.stat, vld.norm.crit] = EXPERIMENT.analysis.validation.normality(sts);    
        
        % homoscedasticity of the residuals for the topics
        [vld.homo.subject.p, vld.homo.subject.stats] = EXPERIMENT.analysis.validation.homoscedasticity(sts, subject);
        
        % homoscedasticity of the residuals for the systems
        [vld.homo.factorA.p, vld.homo.factorA.stats] = EXPERIMENT.analysis.validation.homoscedasticity(sts, factorA);
                
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        anovaStatsID = EXPERIMENT.pattern.identifier.anova.sts(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        anovaVldID = EXPERIMENT.pattern.identifier.anova.vld(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(TAG, balanced, sstype, quartile, mid, corpusID, trackID);
        
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'me', 'tbl', 'sts', 'blc', 'vld', 'soa', 'mc'}, ...
                'FileVarNames', {anovaMeID, anovaTableID, anovaStatsID, anovaBlcID, anovaVldID, anovaSoAID, anovaMcID});
                
        clear data me tbl sts blc vld soa mc;
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
                     
    end % for measure
                   
    fprintf('\n\n######## Total elapsed time for performing %s ANOVA analysis on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end

