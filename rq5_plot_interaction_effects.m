%% rq5_plot_interaction_effects
% 
% Plots the interaction effects for the Topic/System/Shard Effects ANOVA.

%% Synopsis
%
%   [] = rq5_plot_interaction_effects(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|balanced|* - the type of design to enforce. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros;
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq5_plot_interaction_effects(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'rq5';
    end
    
    % check the number of input parameters
    narginchk(5, 7);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
              
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);

    % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');
        
    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';
    
    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');
     
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
    % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');
        
    if nargin == 7
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end    
     
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s interactio  effects on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    
     % for each measure
    for m = startMeasure:endMeasure
        
        fprintf('\n+ Plotting interaction effects for %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaIeID = EXPERIMENT.pattern.identifier.anova.ie(TAG, balanced, sstype, quartile, mid, splitID, trackID);
           
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'ie'}, ...
                'FileVarNames', {anovaIeID});
            
        
        currentFigure = figure('Visible', 'off');
        
        data.mean = ie.factorBA.mean.';

        hold on;

        x = 1:length(ie.factorBA.factorB.label);
            
        colors = parula(length(ie.factorBA.factorA.label));

        % for each system
        for l = 1:length(ie.factorBA.factorA.label)        
            plot(x, data.mean(l, :), 'Color', colors(l, :), 'LineWidth', 1.5);
        end

        ax = gca;
        ax.TickLabelInterpreter = 'latex';
        ax.FontSize = 24;

        if EXPERIMENT.split.(splitID).shard < 10
            ax.XTick = 1:EXPERIMENT.split.(splitID).shard;
            ax.XTickLabel = EXPERIMENT.split.(splitID).shardLabels;
        else        
            %ax.XTick = [1 5:5:length(ie.factorBA.factorB.label)];
            ax.XTick = [];
            %ax.XTickLabel = cellfun(@(x) sprintf('\\texttt{%s}', x), me.shard, 'UniformOutput', false);
        end
        ax.TickLabelInterpreter = 'latex';
                        
        %ax.XTickLabelRotation = 90;

        ax.XLabel.Interpreter = 'latex';
        %ax.XLabel.String = sprintf('\\texttt{%s} Shards', strrep(splitID, '_', '\_'));     
        ax.XLabel.String = sprintf('Shards');     

        ax.YLabel.Interpreter = 'latex';
        ax.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
        
        if EXPERIMENT.split.(splitID).shard < 10
            ax.XLim = [1 EXPERIMENT.split.(splitID).shard];
        else
            ax.XLim = [1 length(ie.factorBA.factorB.label)];
        end
                        
        ax.YLim = [0 1];

        title(sprintf('%1$s*%2$s Interaction Effects - Each line is a \\texttt{%3$s} %2$s', EXPERIMENT.analysis.label.factorB, EXPERIMENT.analysis.label.factorA, strrep(trackID, '_', '\_'), strrep(splitID, '_', '\_')), 'FontSize', 24, 'Interpreter', 'latex');
                       
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 22];
        currentFigure.PaperPosition = [1 1 40 20];

        figureID = EXPERIMENT.pattern.identifier.anova.iePlot(TAG, lower(EXPERIMENT.analysis.label.factorB), lower(EXPERIMENT.analysis.label.factorA), balanced, sstype, quartile, mid, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
        
        
        
        
        currentFigure = figure('Visible', 'off');
        
        data.mean = ie.factorAB.mean.';

        hold on;

        x = 1:length(ie.factorAB.factorA.label);
            
        colors = parula(length(ie.factorAB.factorB.label));

        % for each shard
        for l = 1:length(ie.factorAB.factorB.label)        
            plot(x, data.mean(l, :), 'Color', colors(l, :), 'LineWidth', 1.5);
        end

        ax = gca;
        ax.TickLabelInterpreter = 'latex';
        ax.FontSize = 24;
                
        %ax.XTick = [1 5:5:length(ie.factorAB.factorA.label)];
        ax.XTick = [];
        %ax.XTickLabel = cellfun(@(x) sprintf('\\texttt{%s}', x), me.shard, 'UniformOutput', false);
        ax.TickLabelInterpreter = 'latex';
        ax.XTickLabelRotation = 90;

        ax.XLabel.Interpreter = 'latex';
        %ax.XLabel.String = sprintf('\\texttt{%s} Systems', strrep(trackID, '_', '\_'));
        ax.XLabel.String = sprintf('Systems');

        ax.YLabel.Interpreter = 'latex';
        ax.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
        
        ax.XLim = [1 length(ie.factorBA.factorA.label)];
        ax.YLim = [0 1];

        title(sprintf('%1$s*%2$s Interaction Effects - Each line is a \\texttt{%3$s} %2$s', EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB, strrep(splitID, '_', '\_')), 'FontSize', 24, 'Interpreter', 'latex');
                       
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 22];
        currentFigure.PaperPosition = [1 1 40 20];

        figureID = EXPERIMENT.pattern.identifier.anova.iePlot(TAG, lower(EXPERIMENT.analysis.label.factorA), lower(EXPERIMENT.analysis.label.factorB), balanced, sstype, quartile, mid, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
    
        
        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaIeID = EXPERIMENT.pattern.identifier.anova.ie(TAG, balanced, sstype, quartile, mid, splitID, trackID);
           
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'ie'}, ...
                'FileVarNames', {anovaIeID});
            
        
        currentFigure = figure('Visible', 'off');
        
        data.mean = ie.factorBSubject.mean.';

        hold on;

        x = 1:length(ie.factorBSubject.factorB.label);
            
        colors = parula(length(ie.factorBSubject.subject.label));

        % for each system
        for l = 1:length(ie.factorBSubject.subject.label)        
            plot(x, data.mean(l, :), 'Color', colors(l, :), 'LineWidth', 1.5);
        end

        ax = gca;
        ax.TickLabelInterpreter = 'latex';
        ax.FontSize = 24;

        if EXPERIMENT.split.(splitID).shard < 10
            ax.XTick = 1:EXPERIMENT.split.(splitID).shard;
            ax.XTickLabel = EXPERIMENT.split.(splitID).shardLabels;
        else        
            %ax.XTick = [1 5:5:length(ie.factorBSubject.factorB.label)];
            ax.XTick = [];
            %ax.XTickLabel = cellfun(@(x) sprintf('\\texttt{%s}', x), me.shard, 'UniformOutput', false);
        end
        ax.TickLabelInterpreter = 'latex';
                        
        %ax.XTickLabelRotation = 90;

        ax.XLabel.Interpreter = 'latex';
        %ax.XLabel.String = sprintf('\\texttt{%s} Shards', strrep(splitID, '_', '\_'));     
        ax.XLabel.String = sprintf('Shards');     

        ax.YLabel.Interpreter = 'latex';
        ax.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
        
        if EXPERIMENT.split.(splitID).shard < 10
            ax.XLim = [1 EXPERIMENT.split.(splitID).shard];
        else
            ax.XLim = [1 length(ie.factorBSubject.factorB.label)];
        end
                        
        ax.YLim = [0 1];

        title(sprintf('%1$s*%2$s Interaction Effects - Each line is a \\texttt{%3$s} %2$s', EXPERIMENT.analysis.label.factorB, EXPERIMENT.analysis.label.subject, strrep(trackID, '_', '\_'), strrep(splitID, '_', '\_')), 'FontSize', 24, 'Interpreter', 'latex');
                       
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 22];
        currentFigure.PaperPosition = [1 1 40 20];

        figureID = EXPERIMENT.pattern.identifier.anova.iePlot(TAG, lower(EXPERIMENT.analysis.label.factorB), lower(EXPERIMENT.analysis.label.subject), balanced, sstype, quartile, mid, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
        
        
        
        
        currentFigure = figure('Visible', 'off');
        
        data.mean = ie.subjectfactorB.mean.';

        hold on;

        x = 1:length(ie.subjectfactorB.subject.label);
            
        colors = parula(length(ie.subjectfactorB.factorB.label));

        % for each shard
        for l = 1:length(ie.subjectfactorB.factorB.label)        
            plot(x, data.mean(l, :), 'Color', colors(l, :), 'LineWidth', 1.5);
        end

        ax = gca;
        ax.TickLabelInterpreter = 'latex';
        ax.FontSize = 24;
                
        %ax.XTick = [1 5:5:length(ie.subjectfactorB.subject.label)];
        ax.XTick = [];
        %ax.XTickLabel = cellfun(@(x) sprintf('\\texttt{%s}', x), me.shard, 'UniformOutput', false);
        ax.TickLabelInterpreter = 'latex';
        ax.XTickLabelRotation = 90;

        ax.XLabel.Interpreter = 'latex';
        %ax.XLabel.String = sprintf('\\texttt{%s} Systems', strrep(trackID, '_', '\_'));
        ax.XLabel.String = sprintf('Topics');

        ax.YLabel.Interpreter = 'latex';
        ax.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
        
        ax.XLim = [1 length(ie.factorBSubject.subject.label)];
        ax.YLim = [0 1];

        title(sprintf('%1$s*%2$s Interaction Effects - Each line is a \\texttt{%3$s} %2$s', EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorB, strrep(splitID, '_', '\_')), 'FontSize', 24, 'Interpreter', 'latex');
                       
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 22];
        currentFigure.PaperPosition = [1 1 40 20];

        figureID = EXPERIMENT.pattern.identifier.anova.iePlot(TAG, lower(EXPERIMENT.analysis.label.subject), lower(EXPERIMENT.analysis.label.factorB), balanced, sstype, quartile, mid, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
    
        
        
    	anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, splitID, trackID);
        anovaIeID = EXPERIMENT.pattern.identifier.anova.ie(TAG, balanced, sstype, quartile, mid, splitID, trackID);
           
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'ie'}, ...
                'FileVarNames', {anovaIeID});
            
        
        currentFigure = figure('Visible', 'off');
        
        data.mean = ie.factorASubject.mean.';

        hold on;

        x = 1:length(ie.factorASubject.factorA.label);
            
        colors = parula(length(ie.factorASubject.subject.label));

        % for each system
        for l = 1:length(ie.factorASubject.subject.label)        
            plot(x, data.mean(l, :), 'Color', colors(l, :), 'LineWidth', 1.5);
        end

        ax = gca;
        ax.TickLabelInterpreter = 'latex';
        ax.FontSize = 24;

        if EXPERIMENT.split.(splitID).shard < 10
            ax.XTick = 1:EXPERIMENT.split.(splitID).shard;
            ax.XTickLabel = EXPERIMENT.split.(splitID).shardLabels;
        else        
            %ax.XTick = [1 5:5:length(ie.factorASubject.factorA.label)];
            ax.XTick = [];
            %ax.XTickLabel = cellfun(@(x) sprintf('\\texttt{%s}', x), me.shard, 'UniformOutput', false);
        end
        ax.TickLabelInterpreter = 'latex';
                        
        %ax.XTickLabelRotation = 90;

        ax.XLabel.Interpreter = 'latex';
        %ax.XLabel.String = sprintf('\\texttt{%s} Shards', strrep(splitID, '_', '\_'));     
        ax.XLabel.String = sprintf('Systems');     

        ax.YLabel.Interpreter = 'latex';
        ax.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
        
        if EXPERIMENT.split.(splitID).shard < 10
            ax.XLim = [1 EXPERIMENT.split.(splitID).shard];
        else
            ax.XLim = [1 length(ie.factorASubject.factorA.label)];
        end
                        
        ax.YLim = [0 1];

        title(sprintf('%1$s*%2$s Interaction Effects - Each line is a \\texttt{%3$s} %2$s', EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.subject, strrep(trackID, '_', '\_'), strrep(splitID, '_', '\_')), 'FontSize', 24, 'Interpreter', 'latex');
                       
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 22];
        currentFigure.PaperPosition = [1 1 40 20];

        figureID = EXPERIMENT.pattern.identifier.anova.iePlot(TAG, lower(EXPERIMENT.analysis.label.factorA), lower(EXPERIMENT.analysis.label.subject), balanced, sstype, quartile, mid, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
        
        
        
        
        currentFigure = figure('Visible', 'off');
        
        data.mean = ie.subjectFactorA.mean.';

        hold on;

        x = 1:length(ie.subjectFactorA.subject.label);
            
        colors = parula(length(ie.subjectFactorA.factorA.label));

        % for each shard
        for l = 1:length(ie.subjectFactorA.factorA.label)        
            plot(x, data.mean(l, :), 'Color', colors(l, :), 'LineWidth', 1.5);
        end

        ax = gca;
        ax.TickLabelInterpreter = 'latex';
        ax.FontSize = 24;
                
        %ax.XTick = [1 5:5:length(ie.subjectFactorA.subject.label)];
        ax.XTick = [];
        %ax.XTickLabel = cellfun(@(x) sprintf('\\texttt{%s}', x), me.shard, 'UniformOutput', false);
        ax.TickLabelInterpreter = 'latex';
        ax.XTickLabelRotation = 90;

        ax.XLabel.Interpreter = 'latex';
        %ax.XLabel.String = sprintf('\\texttt{%s} Systems', strrep(trackID, '_', '\_'));
        ax.XLabel.String = sprintf('Topics');

        ax.YLabel.Interpreter = 'latex';
        ax.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
        
        ax.XLim = [1 length(ie.factorASubject.subject.label)];
        ax.YLim = [0 1];

        title(sprintf('%1$s*%2$s Interaction Effects - Each line is a \\texttt{%3$s} %2$s', EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, strrep(splitID, '_', '\_')), 'FontSize', 24, 'Interpreter', 'latex');
                       
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 22];
        currentFigure.PaperPosition = [1 1 40 20];

        figureID = EXPERIMENT.pattern.identifier.anova.iePlot(TAG, lower(EXPERIMENT.analysis.label.subject), lower(EXPERIMENT.analysis.label.factorA), balanced, sstype, quartile, mid, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
    
        
        
        
        
        
        
        
    end % measure
           
    fprintf('\n\n######## Total elapsed time for plotting %s shard interaction effects on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
end

