%% compute_quartile
% 
% Computes the indexes of the systems to be kept according to the requested
% quartile
%
%% Synopsis
%
%   [idx] = compute_quartile(quartile, measure)
%  
% *Parameters*
%
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% * *|measure|* - the measure whose quartile has to be determined.
%
%
% *Returns*
%
% * *|idx|* - the indexes of the systems which fall in the requested
% quartile for the given measure.


%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [idx] = compute_quartile(quartile, measure)

    % assume a balanced design, i.e. no NaN in the measure computed on
    % the whole corpus
    assert(~any(any(isnan(measure{:, :}))), ...
        'MATTERS:IllegalState', 'A measure is expected to not contain any NaN value when computed on the whole corpus.');

    % order systems by descending mean of the measure on the whole
    % corpus
    [~, idx] = sort(mean(measure{:, :}), 'descend');

    switch quartile
        case 1
            % keep only runs in the top quartile
            idx = idx(1:floor(length(idx)/4));
        case 2
            % keep only runs in the top two quartiles, i.e. above median
            idx = idx(1:floor(length(idx)/2));
        case 3
            % keep only runs in the top three quartiles
            idx = idx(1:floor(length(idx)/4*3));
        case 4
            % nothing to do, keep all the runs
    end
end
