%% rq5_plot_omega2p_measure_cutoff
% 
% Plots the omega2p for different effects at different measure cut-offs.

%% Synopsis
%
%   [] = rq5_plot_omega2p_measure_cutoff(trackID, splitID, balanced, sstype, quartile, measure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|balanced|* - the type of design to enforce. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros;
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use 1 for top
% quartile; 2 for median; 3 for up to third quartile; 4 for all systems.
% * *|measure|* - |precision| for omega2p using precision at cut-off;
% |ndgc| for omega2p using nDCD at cut-off.

%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = rq5_plot_omega2p_measure_cutoff(trackID, splitID, balanced, sstype, quartile, measure)

    persistent TAG PREC NDCG LINE_WIDTH MARKER_SIZE;
    
    if isempty(TAG)
        TAG = 'rq5';      
        PREC = {'p5', 'p10', 'p20', 'p50', 'p100'};
        NDCG = {'ndcg5', 'ndcg10', 'ndcg20', 'ndcg50', 'ndcg100'};
        LINE_WIDTH = 2.5;
        MARKER_SIZE = 15;
        
    end

    % check the number of input parameters
    narginchk(6, 6);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
        
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);

    % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');
        
    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';
    
    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');
     
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
    % check that quartile is an integer with possible values 1, 2, 3, and 4.
    validateattributes(quartile, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 4}, '', 'quartile');
    
    % check that measure is a non-empty cell array
    validateattributes(measure, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'measure');
    
    if iscell(measure)
        % check that measure is a cell array of strings with one element
        assert(iscellstr(measure) && numel(measure) == 1, ...
            'MATTERS:IllegalArgument', 'Expected measure to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    measure = char(lower(strtrim(measure)));
    measure = measure(:).';
    
    % check that measure assumes a valid value
    validatestring(measure, ...
        {'prec', 'ndcg'}, '', 'measure');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
     
    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting %s omega2p on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %d\n', quartile);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    fprintf('  - measures: %s\n', measure);
    
    if measure == 'prec'
        mids = PREC;
    else
        mids = NDCG;
    end        
        
    % for each measure
    for m = 1:length(mids)
        
        mid = mids{m};
        labels{m} = EXPERIMENT.measure.(mid).acronym;
        
        try
            
            % topic/system/shard
            anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, splitID, trackID);
            anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, balanced, sstype, quartile, mid, splitID, trackID);
            anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, balanced, sstype, quartile, mid, splitID, trackID);
            anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG, balanced, sstype, quartile, mid, splitID, trackID);
            
            serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'tbl5', 'soa5', 'me5'}, ...
                'FileVarNames', {anovaTableID, anovaSoAID, anovaMeID});
            
        catch
            
            data.topic.p(m) = NaN;
            data.topic.soa(m) = NaN;
            
            data.system.p(m) = NaN;
            data.system.soa(m) = NaN;
            
            data.shard.p(m) = NaN;
            data.shard.soa(m) = NaN;
            
            data.system_shard.p(m) = NaN;
            data.system_shard.soa(m) = NaN;
            
            data.topic_system.p(m) = NaN;
            data.topic_system.soa(m) = NaN;
            
            data.topic_shard.p(m) = NaN;
            data.topic_shard.soa(m) = NaN;
            
            
            continue;
        end
        
        
        data.topic.p(m) = tbl5{2, 7};
        data.topic.soa(m) = soa5.omega2p.subject;
        
        data.system.p(m) = tbl5{3, 7};
        data.system.soa(m) = soa5.omega2p.factorA;
        
        data.shard.p(m) = tbl5{4, 7};
        data.shard.soa(m) = soa5.omega2p.factorB;
        
        data.system_shard.p(m) = tbl5{5, 7};
        data.system_shard.soa(m) = soa5.omega2p.factorA_factorB;
        
        data.topic_system.p(m) = tbl5{6, 7};
        data.topic_system.soa(m) = soa5.omega2p.subject_factorA;
        
        data.topic_shard.p(m) = tbl5{7, 7};
        data.topic_shard.soa(m) = soa5.omega2p.subject_factorB;
        
        clear me1 tbl5 soa5 me5 tau;
    end    % measure
    
    % remove omega2p for not significant effects
    data.topic.soa(data.topic.p >= EXPERIMENT.analysis.alpha.threshold) = NaN;
    data.system.soa(data.system.p >= EXPERIMENT.analysis.alpha.threshold) = NaN;
    data.shard.soa(data.shard.p >= EXPERIMENT.analysis.alpha.threshold) = NaN;
    data.system_shard.soa(data.system_shard.p >= EXPERIMENT.analysis.alpha.threshold) = NaN;
    data.topic_system.soa(data.topic_system.p >= EXPERIMENT.analysis.alpha.threshold) = NaN;
    data.topic_shard.soa(data.topic_shard.p >= EXPERIMENT.analysis.alpha.threshold) = NaN;
    
    
    currentFigure = figure('Visible', 'off');
    
    xTick = 1:length(mids);
    
    plot(xTick, data.topic.soa, ...
            'LineStyle', ':',  'LineWidth', LINE_WIDTH, 'Marker', 's', 'MarkerSize', MARKER_SIZE, ...
            'Color', rgb('FireBrick'), ...
            'MarkerFaceColor', rgb('FireBrick'), ...
            'MarkerEdgeColor', rgb('FireBrick'));
   
    hold on
    
    plot(xTick, data.system.soa, ...
            'LineStyle', ':',  'LineWidth', LINE_WIDTH, 'Marker', 'o', 'MarkerSize', MARKER_SIZE, ...
            'Color', rgb('RoyalBlue'), ...
            'MarkerFaceColor', rgb('RoyalBlue'), ...
            'MarkerEdgeColor', rgb('RoyalBlue'));        
        
    
    plot(xTick, data.shard.soa, ...
            'LineStyle', ':',  'LineWidth', LINE_WIDTH, 'Marker', 'd', 'MarkerSize', MARKER_SIZE, ...
            'Color', rgb('DarkGreen'), ...
            'MarkerFaceColor', rgb('DarkGreen'), ...
            'MarkerEdgeColor', rgb('DarkGreen'));            
    
    plot(xTick, data.system_shard.soa, ...
            'LineStyle', ':',  'LineWidth', LINE_WIDTH, 'Marker', 'x', 'MarkerSize', MARKER_SIZE, ...
            'Color', rgb('Chocolate'), ...
            'MarkerFaceColor', rgb('Chocolate'), ...
            'MarkerEdgeColor', rgb('Chocolate'));            

    plot(xTick, data.topic_system.soa, ...
            'LineStyle', ':',  'LineWidth', LINE_WIDTH, 'Marker', '+', 'MarkerSize', MARKER_SIZE, ...
            'Color', rgb('DarkOrange'), ...
            'MarkerFaceColor', rgb('DarkOrange'), ...
            'MarkerEdgeColor', rgb('DarkOrange'));            

        
    plot(xTick, data.topic_shard.soa, ...
            'LineStyle', ':',  'LineWidth', 3.5, 'Marker', '*', 'MarkerSize', MARKER_SIZE, ...
            'Color', rgb('DarkViolet'), ...
            'MarkerFaceColor', rgb('DarkViolet'), ...
            'MarkerEdgeColor', rgb('DarkViolet'));            
        
    
    ax = gca;
    ax.TickLabelInterpreter = 'latex';
    ax.FontSize = 24;
    
    ax.XTick = xTick;
    ax.XTickLabel = labels;
    ax.TickLabelInterpreter = 'latex';
    
    ax.XLabel.Interpreter = 'latex';
     if measure == 'prec'
        ax.XLabel.String = sprintf('Precision');
    else
        ax.XLabel.String = sprintf('Normalized Discounted Cumulated Gain');
    end   
        
    ax.YLabel.Interpreter = 'latex';
    ax.YLabel.String = sprintf('$\\hat{\\omega}^2$');
    
%    ax.XLim = [1 length(me.factorA.label)];
    ax.YLim = [0 1];
    
    
    title(sprintf('Effect size on \\texttt{%s} -- \\texttt{%s}', strrep(trackID, '_', '\_'), strrep(splitID, '_', '\_')), 'FontSize', 24, 'Interpreter', 'latex');
    
%     legend({sprintf('$\\hat{\\omega}^2_{\\langle\\text{%s}\\rangle}$', EXPERIMENT.analysis.label.subject), ...
%             sprintf('$\\hat{\\omega}^2_{\\langle\\text{%s}\\rangle}$', EXPERIMENT.analysis.label.factorA), ...
%             sprintf('$\\hat{\\omega}^2_{\\langle\\text{%s}\\rangle}$', EXPERIMENT.analysis.label.factorB), ...
%             sprintf('$\\hat{\\omega}^2_{\\langle\\text{%s*%s}\\rangle}$', EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB), ...
%             sprintf('$\\hat{\\omega}^2_{\\langle\\text{%s*%s}\\rangle}$', EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA), ...
%             sprintf('$\\hat{\\omega}^2_{\\langle\\text{%s*%s}\\rangle}$', EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorB)                    
%             }, 'Location', 'northeastoutside', 'FontSize', 24, 'Interpreter', 'tex' );

    legend({sprintf('%s', EXPERIMENT.analysis.label.subject), ...
            sprintf('%s', EXPERIMENT.analysis.label.factorA), ...
            sprintf('%s', EXPERIMENT.analysis.label.factorB), ...
            sprintf('%s*%s', EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB), ...
            sprintf('%s*%s', EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA), ...
            sprintf('%s*%s', EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorB)                    
            }, 'Location', 'northeastoutside', 'FontSize', 24, 'Interpreter', 'latex' );
 

    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [42 22];
    currentFigure.PaperPosition = [1 1 40 20];
    
    figureID = EXPERIMENT.pattern.identifier.anova.soaPlot(TAG, balanced, sstype, quartile, measure, splitID, trackID);
    
    print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));
    
    close(currentFigure)
    
    fprintf('\n\n######## Total elapsed time for plotting %s omega2p on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
end

