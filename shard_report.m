%% shard_report
% 
% Reports statistics about the shards of a split for the given
% track and saves them to a |.tex| file.
%
%% Synopsis
%
%   [] = shard_report(trackID, splitID)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = shard_report(trackID, splitID)

    persistent DUMMY_DOC;
    
    if isempty(DUMMY_DOC)
        DUMMY_DOC = '#####DUMMY_DOC_ID^^^^^^';
    end
    
    % check the number of input parameters
    narginchk(2, 2);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
    
    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
    
    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';
    
    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');
    
    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
    
    
    % start of overall computations
    startComputation = tic;
    
    
    fprintf('\n\n######## Reporting %s shard statistics for track %s (%s) ########\n\n', ...
        splitID, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - track %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('  - shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    
    
    fprintf('+ Printing the report\n');
    fprintf('  ');
    
    % the file where the report has to be written
    reportID = EXPERIMENT.pattern.identifier.rep.shard(splitID, trackID);
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, reportID), 'w');
    
    
    fprintf(fid, '\\documentclass[11pt]{article} \n\n');
    
    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{longtable}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    %fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Summary Report on Shard Statistics \\\\ Split %s on track %s}\n\n', strrep(splitID, '_', '\_'), trackID);
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    fprintf(fid, 'Settings:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item track: %s -- %s \n', trackID, EXPERIMENT.track.(trackID).name);
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID).topics);
    fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID).runs);
    fprintf(fid, '\\end{itemize}\n');
    fprintf(fid, '\\item corpus: %s -- %s \n', strrep(EXPERIMENT.split.(splitID).corpus, '_', '\_'), EXPERIMENT.corpus.(EXPERIMENT.split.(splitID).corpus).name);
    fprintf(fid, '\\item split: %s -- %s \n', strrep(splitID, '_', '\_'), strrep(EXPERIMENT.split.(splitID).name, '_', '\_'));
    fprintf(fid, '\\end{itemize}\n\n');
    
    fprintf(fid, 'Statistics about each topic of a pool:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item \\textbf{\\# Shards No Rel.}: total number of shards without any relevant document. These shards cause issues because, without any relevant document, you cannot compute any measure on them, that is any measure will be \\texttt{NaN}.\n');
    fprintf(fid, '\\item \\textbf{\\# Shards At Least One Rel.}: number of shards with at least one relevant document. These shards do not cause issues because they allow for a correct computation of a measure.\n');
    fprintf(fid, '\\item \\textbf{\\# Shards No Pooled Docs}: number of shards without any pooled documents. These shards cause issues because the missing pooled documents will be considered as not relevant documents. Therefore, you cannot compute any measure on these shards, that is any measure will be \\texttt{NaN}. \n');
    fprintf(fid, '\\end{itemize}\n\n');
    
    fprintf(fid, 'Note that the sum of the above statistics will add up to the total number of shards in the split.\n\n');
    
    fprintf(fid, 'Statistics about each topic of a set of runs:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item \\textbf{\\# Shards No Runs Retrieving Any Docs}: total number of shards where no runs retrieve any document from that shard. These shards will score 0 for all the runs for those topics where it is possible to compute a measure.\n');
    fprintf(fid, '\\item \\textbf{\\# Runs Not Retrieving Docs in at Least One Shard}: total number of runs not retrieving any document in at least one shard. These runs will score 0 on those shards for all the runs for those topics where it is possible to compute a measure. Note that the previous statistics is a special case of this one, i.e. when all the runs do not retrieve any document; this statistics contains all this number.\n');
    fprintf(fid, '\\item \\textbf{Avg. \\# Runs Not Retrieving Docs}: it is the average of the previous staticts by the total number of shards in the split.\n');
    fprintf(fid, '\\end{itemize}\n\n');
    
    fprintf(fid, '\\newpage\n');
    
    
    corpusPoolID = EXPERIMENT.pattern.identifier.pool.corpus(EXPERIMENT.split.(splitID).corpus, trackID);
    
    serload2(EXPERIMENT.pattern.file.dataset.corpus(trackID, corpusPoolID), ...
        'WorkspaceVarNames', {'corpusPool'}, ...
        'FileVarNames', {corpusPoolID});
    
    % the total number of topics in the pool
    topics = corpusPool.Properties.RowNames;
    T = height(corpusPool);
    
    relevanceDegrees = categories(corpusPool{:, 1}{1, 1}(:, :).RelevanceDegree);
    
    clear corpusPool;
    
    % for each topic, it keeps the following statistics about a pool:
    % number of shards without any relevant document; number of shards
    % with at least one relevant document; number of shards without any
    % pooled documents
    counter.pool.onlyNotRelevantDocs = zeros(1, T);
    counter.pool.atLeastOneRelevantDoc = zeros(1, T);
    counter.pool.noPooledDocs = zeros(1, T);
    counter.pool.data = zeros(T, EXPERIMENT.split.(splitID).shard);
    
    
    % for each topic, it keeps the following statistics about a run set:
    % number of shards where no runs retrieve any document from that shard;
    % number of runs not retrieving any document in at least one shard
    counter.run.noRunsRetrievingDocs = zeros(1, T);
    counter.run.noRetrievedDocsAtLeastOneShard = zeros(1, T);
    
    
    fprintf('  - processing shard(s)\n');
    fprintf('      ');
    
    % for each shard, pool and run statistics
    for sc = 1:EXPERIMENT.split.(splitID).shard
        
        shardID = EXPERIMENT.pattern.identifier.shard(sc);
        
        shardPoolID = EXPERIMENT.pattern.identifier.pool.shard(splitID, shardID, trackID);
        serload2(EXPERIMENT.pattern.file.dataset.shard(trackID, splitID, shardPoolID), ...
            'WorkspaceVarNames', {'pool'}, ...
            'FileVarNames', {shardPoolID});
        
        
        shardRunID = EXPERIMENT.pattern.identifier.run.shard(splitID, shardID, trackID);
        serload2(EXPERIMENT.pattern.file.dataset.shard(trackID, splitID, shardRunID), ...
            'WorkspaceVarNames', {'runSet'}, ...
            'FileVarNames', {shardRunID});
        
        % the total number of runs in the set
        R = width(runSet);
        
        % for each topic
        for t = 1:T
            
            % extract the inner pool table contained in the topic
            poolTopic = pool{t, 1}{1, 1};
                        
            % find the positions of the not relevant documents
            notRelevantDocs = poolTopic.RelevanceDegree == relevanceDegrees(1);
            
            if all(ismember(poolTopic.Document, DUMMY_DOC))
                counter.pool.noPooledDocs(t) = counter.pool.noPooledDocs(t) + 1;
                
                % find the total number of relevant documents for that topic
                % and shard
                counter.pool.data(t, sc) = 0;
            else
                if all(notRelevantDocs)
                    counter.pool.onlyNotRelevantDocs(t) = counter.pool.onlyNotRelevantDocs(t) + 1;
                else
                    counter.pool.atLeastOneRelevantDoc(t) = counter.pool.atLeastOneRelevantDoc(t) + 1;
                end
                
                % find the total number of relevant documents for that topic
                % and shard
                counter.pool.data(t, sc) = sum(~notRelevantDocs);
                
                
            end
            
            noRetrievedDocs = false(1, R);
            
            % for each run
            for r = 1:R
                
                runTopic = runSet{t, r}{1, 1};
                
                if height(runTopic) == 0
                    warning('No documents for topic %d, run %d in shard %s.', t, r, shardID);
                end
                
                if any(ismember(runTopic.Document, DUMMY_DOC))
                    noRetrievedDocs(r) = true;
                end
            end % for run
            
            if all(noRetrievedDocs)
                counter.run.noRunsRetrievingDocs(t) = counter.run.noRunsRetrievingDocs(t) + 1;
            end
            
            counter.run.noRetrievedDocsAtLeastOneShard(t) = counter.run.noRetrievedDocsAtLeastOneShard(t) + sum(noRetrievedDocs);
            
        end % for topic
        
        clear pool runSet;
        
        fprintf('.');
        
    end % for shard
    
    fprintf('\n');
    fprintf('  - saving shard statistics\n');
    
    shardStatsID = EXPERIMENT.pattern.identifier.stats.shard(splitID, trackID);
    
    sersave2(EXPERIMENT.pattern.file.analysis(trackID, shardStatsID), ...
        'WorkspaceVarNames', {'counter'}, ...
        'FileVarNames', {shardStatsID});
            
    fprintf('  - writing summary tables\n');
    
    fprintf(fid, '\\begin{table}[p] \n');
    
    fprintf(fid, '\\centering \n');
    
    fprintf(fid, '\\caption{Shard statistics for \\texttt{%s} pool with \\texttt{%s} shards.}\n', ...
        trackID, strrep(splitID, '_', '\_'));
    
    fprintf(fid, '\\label{tab:shard-pool-stats-%s-%s}\n', trackID, splitID);
    
    fprintf(fid, '\\tiny \n');
    
    %fprintf(fid, '\\hspace*{-6.5em} \n');
    
    fprintf(fid, '\\begin{tabular}{|l|r|r|r|r|} \n');
    
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\multicolumn{1}{|c|}{\\textbf{Topic}} & \\multicolumn{1}{c|}{\\textbf{\\# Shards No Rel.}} & \\multicolumn{1}{c|}{\\textbf{\\# Shards At Least One Rel.}} & \\multicolumn{1}{c|}{\\textbf{\\# Shards No Pooled Docs}} & \\multicolumn{1}{c|}{\\textbf{Total \\# Shards}} \\\\ \n');
    
    fprintf(fid, '\\hline \n');
    
    % for each topic
    for t = 1:T
        fprintf(fid, '\\textbf{%s}      & %d        & %d        & %d       & %d \\\\ \n', ...
            topics{t}, counter.pool.onlyNotRelevantDocs(t), counter.pool.atLeastOneRelevantDoc(t), counter.pool.noPooledDocs(t), ...
            counter.pool.onlyNotRelevantDocs(t) + counter.pool.atLeastOneRelevantDoc(t) + counter.pool.noPooledDocs(t));
        fprintf(fid, '\\hline \n');
    end % for topic
    
    fprintf(fid, '\\textbf{Average}		& %7.2f     & %7.2f     & %7.2f    & -- \\\\ \n', ...
        mean(counter.pool.onlyNotRelevantDocs), mean(counter.pool.atLeastOneRelevantDoc), mean(counter.pool.noPooledDocs));
    
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\end{table} \n\n');
    
    
    
    fprintf(fid, '\\begin{table}[h] \n');
    fprintf(fid, '\\centering \n');
    
    fprintf(fid, '\\caption{Shard statistics for \\texttt{%s} runs with \\texttt{%s} shards.}\n', ...
        trackID, strrep(splitID, '_', '\_'));
    
    fprintf(fid, '\\label{tab:run-stats-%s-%s}\n', trackID, splitID);
    
    fprintf(fid, '\\tiny \n');
    
    fprintf(fid, '\\hspace*{-10em} \n');
    
    fprintf(fid, '\\begin{tabular}{|l|r|r|r|} \n');
    
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\multicolumn{1}{|c|}{\\textbf{Topic}} & \\multicolumn{1}{c|}{\\textbf{\\# Shards No Runs Retrieving Any Docs}} & \\multicolumn{1}{c|}{\\textbf{\\# Runs Not Retrieving Docs in at Least One Shard}} & \\multicolumn{1}{c|}{\\textbf{Avg. \\# Runs Not Retrieving Docs}}  \\\\ \n');
    
    fprintf(fid, '\\hline \n');
    
    % for each topic
    for t = 1:T
        fprintf(fid, '\\textbf{%s}      & %d        & %d    & %12.2f \\\\ \n', ...
            topics{t}, counter.run.noRunsRetrievingDocs(t), counter.run.noRetrievedDocsAtLeastOneShard(t), ...
            counter.run.noRetrievedDocsAtLeastOneShard(t) / EXPERIMENT.split.(splitID).shard );
        fprintf(fid, '\\hline \n');
    end % for topic
    
    fprintf(fid, '\\textbf{Average}		& %12.2f     & %12.2f     & %12.2f \\\\ \n', ...
        mean(counter.run.noRunsRetrievingDocs), mean(counter.run.noRetrievedDocsAtLeastOneShard), ...
        mean(counter.run.noRetrievedDocsAtLeastOneShard) / EXPERIMENT.split.(splitID).shard);
    
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\end{table} \n\n');
    
    fprintf(fid, '\\end{document} \n\n');
    
    fclose(fid);
    
    fprintf('\n\n######## Total elapsed time for reporting %s shard statistics for track %s (%s): %s ########\n\n', ...
        splitID, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

        
end
