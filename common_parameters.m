%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration

% if we are running on the cluster 
if (strcmpi(computer, 'GLNXA64'))    
    addpath(genpath('/nas1/promise/ims/ferro/matters/'))   % ave/eva
    addpath(genpath('/ssd/data/ferro/matters/'))   % grace
end

% The base path
if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster    
   % EXPERIMENT.path.base = '/nas1/promise/ims/ferro/yubin/experiment/'; % ave/eva
   EXPERIMENT.path.base = '/ssd/data/ferro/TOIS_FKS/experiment/';  % grace
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2018/TOIS-FKS/experiment/';
end

% The path for the shards, i.e. a directories containing text files, 
% each one listing documents from a corpus according to some criterion
EXPERIMENT.path.shard = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'shard', filesep);

% The path for the datasets, i.e. the runs and the pools of both original
% tracks and sub-corpora
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);

%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'TOIS FKS 2018';


%% Configuration for Corpora

EXPERIMENT.corpus.list = {'TIPSTER', 'WT10g', 'GOV2'};
EXPERIMENT.corpus.number = length(EXPERIMENT.corpus.list);

% The full TIPSTER corpus
EXPERIMENT.corpus.TIPSTER.id = 'TIPSTER';
EXPERIMENT.corpus.TIPSTER.name = 'TIPSTER, Disk 4-5 minus Congressional Record';
EXPERIMENT.corpus.TIPSTER.size = 528155;

% The full WT10g corpus
EXPERIMENT.corpus.WT10g.id = 'WT10g';
EXPERIMENT.corpus.WT10g.name = 'WT10g, a crawl of Web preserving some desiderablde corpus properties (around 1999-2000)';
EXPERIMENT.corpus.WT10g.size = 1692096;


% The full GOV2 corpus
EXPERIMENT.corpus.GOV2.id = 'GOV2';
EXPERIMENT.corpus.GOV2.name = 'A crawl of .gov sites (early 2004)';
EXPERIMENT.corpus.GOV2.size = 25205179;



%% Configuration for Splits

EXPERIMENT.split.list = {'WT10g_00010', 'WT10g_00048', 'WT10g_ALL_TLD', 'WT10g_ALL_TLD_EVEN_SIZE', 'WT10g_ALL_TLD_RANDOM_SIZE', ...
    'WT10g_ALL_TLD_RANDOM_EVEN_SIZE', 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2', 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5', 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10', 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25', ...
    'WT10g_HALF_TLD', 'WT10g_QUARTER_TLD', 'WT10g_EIGHTH_TLD', ...
    'GOV2_00050', 'GOV2_00199', 'TIPSTER_DS', 'GOV2_PDF_NOTPDF'};
EXPERIMENT.split.number = length(EXPERIMENT.split.list);


% WT10g divided in 10 shards
EXPERIMENT.split.WT10g_00010.id = 'WT10g_00010';
EXPERIMENT.split.WT10g_00010.name =  'WT10g divided in 10 shards, corresponding to groupings of domains';
EXPERIMENT.split.WT10g_00010.shard = 10;
EXPERIMENT.split.WT10g_00010.corpus = 'WT10g';
EXPERIMENT.split.WT10g_00010.label = 'WT10g_00010';

% WT10g divided in 48 shards
EXPERIMENT.split.WT10g_00048.id = 'WT10g_00048';
EXPERIMENT.split.WT10g_00048.name =  'WT10g divided in 48 shards, corresponding to groupings of domains';
EXPERIMENT.split.WT10g_00048.shard = 48;
EXPERIMENT.split.WT10g_00048.corpus = 'WT10g';
EXPERIMENT.split.WT10g_00048.label = 'WT10g_00048';

% WT10g divided in 59 TLD shards
EXPERIMENT.split.WT10g_ALL_TLD.id = 'WT10g_ALL_TLD';
EXPERIMENT.split.WT10g_ALL_TLD.name =  'WT10g divided in 59 shards, corresponding to the all Top Level Domains';
EXPERIMENT.split.WT10g_ALL_TLD.shard = 59;
EXPERIMENT.split.WT10g_ALL_TLD.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD.label = 'WT10g_TLD';

% WT10g divided in 59 even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.id = 'WT10g_ALL_TLD_EVEN_SIZE';
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.name =  'WT10g divided in 59 even size shards, corresponding to all the Top Level Domains';
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.shard = 59;
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.label = 'WT10g_TLD_EVEN';

% WT10g divided in 59 random size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.id = 'WT10g_ALL_TLD_RANDOM_SIZE';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.name =  'WT10g divided in 59 random size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.shard = 59;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.label = 'WT10g_RANDOM_UNEVEN';

% WT10g divided in 59 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.name =  'WT10g divided in 59 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.shard = 59;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.label = 'WT10g_RANDOM_EVEN';

% WT10g divided in 2 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.name =  'WT10g divided in 2 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.shard = 2;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.label = 'WT10g_RANDOM_2';

% WT10g divided in 5 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.name =  'WT10g divided in 5 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.shard = 5;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.label = 'WT10g_RANDOM_5';

% WT10g divided in 10 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.name =  'WT10g divided in 10 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.shard = 10;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.label = 'WT10g_RANDOM_10';

% WT25g divided in 25 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.name =  'WT10g divided in 25 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.shard = 25;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.label = 'WT10g_RANDOM_25';

% WT10g divided in 30 TLD shards
EXPERIMENT.split.WT10g_HALF_TLD.id = 'WT10g_HALF_TLD';
EXPERIMENT.split.WT10g_HALF_TLD.name =  'WT10g divided in 30 shards, corresponding to groupings of the Top Level Domains';
EXPERIMENT.split.WT10g_HALF_TLD.shard = 30;
EXPERIMENT.split.WT10g_HALF_TLD.corpus = 'WT10g';
EXPERIMENT.split.WT10g_HALF_TLD.label = 'WT10g_HALF_TLD';

% WT10g divided in 15 TLD shards
EXPERIMENT.split.WT10g_QUARTER_TLD.id = 'WT10g_QUARTER_TLD';
EXPERIMENT.split.WT10g_QUARTER_TLD.name =  'WT10g divided in 15 shards, corresponding to groupings of the Top Level Domains';
EXPERIMENT.split.WT10g_QUARTER_TLD.shard = 15;
EXPERIMENT.split.WT10g_QUARTER_TLD.corpus = 'WT10g';
EXPERIMENT.split.WT10g_QUARTER_TLD.label = 'WT10g_QUARTER_TLD';

% WT10g divided in 8 TLD shards
EXPERIMENT.split.WT10g_EIGHTH_TLD.id = 'WT10g_EIGHTH_TLD';
EXPERIMENT.split.WT10g_EIGHTH_TLD.name =  'WT10g divided in 8 shards, corresponding to groupings of the Top Level Domains';
EXPERIMENT.split.WT10g_EIGHTH_TLD.shard = 8;
EXPERIMENT.split.WT10g_EIGHTH_TLD.corpus = 'WT10g';
EXPERIMENT.split.WT10g_EIGHTH_TLD.label = 'WT10g_EIGHTH_TLD';


% GOV2 divided in 50 shards
EXPERIMENT.split.GOV2_00050.id = 'GOV2_00050';
EXPERIMENT.split.GOV2_00050.name =  'GOV2 divided in 50 shards';
EXPERIMENT.split.GOV2_00050.shard = 50;
EXPERIMENT.split.GOV2_00050.corpus = 'GOV2';
EXPERIMENT.split.GOV2_00050.label = 'GOV2_50';

% GOV2 divided in 199 shards
EXPERIMENT.split.GOV2_00199.id = 'GOV2_00199';
EXPERIMENT.split.GOV2_00199.name =  'GOV2 divided in 199 shards';
EXPERIMENT.split.GOV2_00199.shard = 199;
EXPERIMENT.split.GOV2_00199.corpus = 'GOV2';
EXPERIMENT.split.GOV2_00199.label = 'GOV2_199';

% GOV2 divided in PDF and not PDF documents
EXPERIMENT.split.GOV2_PDF_NOTPDF.id = 'GOV2_PDF_NOTPDF';
EXPERIMENT.split.GOV2_PDF_NOTPDF.name =  'GOV2 divided in 2 shards, PDF and not PDF documents';
EXPERIMENT.split.GOV2_PDF_NOTPDF.shard = 2;
EXPERIMENT.split.GOV2_PDF_NOTPDF.shardLabels = {'GOV2_PDF', 'GOV2_NOTPDF'};
EXPERIMENT.split.GOV2_PDF_NOTPDF.shardNames = {'GOV2, PDF documents', 'GOV2, not PDF documents'};
EXPERIMENT.split.GOV2_PDF_NOTPDF.corpus = 'GOV2';
EXPERIMENT.split.GOV2_PDF_NOTPDF.label = 'GOV2_PDF_NOTPDF';

% TIPSTER divided in 4 shards by document source
EXPERIMENT.split.TIPSTER_DS.id = 'TIPSTER_DS';
EXPERIMENT.split.TIPSTER_DS.name =  'TIPSTER divided by document source';
EXPERIMENT.split.TIPSTER_DS.shard = 4;
EXPERIMENT.split.TIPSTER_DS.shardLabels = {'TIPFBIS', 'TIPFR', 'TIPFT', 'TIPLA'};
EXPERIMENT.split.TIPSTER_DS.shardNames = {'TIPSTER, Foreign Broadcast Information Service', 'TIPSTER, Financial Register', 'TIPSTER, Financial Times', 'TIPSTER, Los Angeles Times'};
EXPERIMENT.split.TIPSTER_DS.corpus = 'TIPSTER';
EXPERIMENT.split.TIPSTER_DS.label = 'TIPSTER_DS';

% Returns the label of a shard in its split, given its index
EXPERIMENT.split.getShardLabel = @(splitID, idx) ( EXPERIMENT.split.(splitID).shardLabels{idx} ); 

% Returns the name of a shard in its split, given its index
EXPERIMENT.split.getShardName = @(splitID, idx) ( EXPERIMENT.split.(splitID).shardNames{idx} ); 

%% Configuration for Tracks

EXPERIMENT.track.list = {'T07', 'T08', 'T09', 'T10', 'T13', 'T14', 'T15', 'T13_50s', 'T14_50s', 'T15_50s', 'T13_199s', 'T14_199s', 'T15_199s'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);

% TREC 07, 1998, Adhoc
EXPERIMENT.track.T07.id = 'T07';
EXPERIMENT.track.T07.name = 'TREC 07, 1998, Adhoc';
EXPERIMENT.track.T07.corpus = 'TIPSTER';
EXPERIMENT.track.T07.topics = 50;
EXPERIMENT.track.T07.runs = 103;

% TREC 08, 1999, Adhoc
EXPERIMENT.track.T08.id = 'T08';
EXPERIMENT.track.T08.name = 'TREC 08, 1999, Adhoc';
EXPERIMENT.track.T08.corpus = 'TIPSTER';
EXPERIMENT.track.T08.topics = 50;
EXPERIMENT.track.T08.runs = 129;

% TREC 09, 2000, Web
EXPERIMENT.track.T09.id = 'T09';
EXPERIMENT.track.T09.name = 'TREC 09, 2000, Web';
EXPERIMENT.track.T09.corpus = 'WT10g';
EXPERIMENT.track.T09.topics = 50;
EXPERIMENT.track.T09.runs = 104;
EXPERIMENT.track.T09.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_09_2000_Web/pool/qrels.trec9.main_web.txt';
EXPERIMENT.track.T09.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T09.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T09.pool.delimiter = 'space';
EXPERIMENT.track.T09.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T09.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T09.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T09.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T09.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_09_2000_Web/runs/all';
EXPERIMENT.track.T09.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T09.run.singlePrecision = true;
EXPERIMENT.track.T09.run.delimiter = 'tab';
EXPERIMENT.track.T09.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T09.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T09.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T09.run.delimiter, 'Verbose', false);

% TREC 10, 2001, Web
EXPERIMENT.track.T10.id = 'T10';
EXPERIMENT.track.T10.name = 'TREC 10, 2001, Web';
EXPERIMENT.track.T10.corpus = 'WT10g';
EXPERIMENT.track.T10.topics = 50;
EXPERIMENT.track.T10.runs = 97;
EXPERIMENT.track.T10.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_10_2001_Web/pool/adhoc_trec2001_qrels.txt';
EXPERIMENT.track.T10.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T10.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T10.pool.delimiter = 'space';
EXPERIMENT.track.T10.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T10.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T10.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T10.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T10.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_10_2001_Web/runs/all';
EXPERIMENT.track.T10.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T10.run.singlePrecision = true;
EXPERIMENT.track.T10.run.delimiter = 'tab';
EXPERIMENT.track.T10.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T10.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T10.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T10.run.delimiter, 'Verbose', false);

% TREC 13, 2004, Terabyte
EXPERIMENT.track.T13.id = 'T13';
EXPERIMENT.track.T13.name =  'TREC 13, 2004, Terabyte';
EXPERIMENT.track.T13.corpus = 'GOV2';
EXPERIMENT.track.T13.topics = 49;
EXPERIMENT.track.T13.runs = 70;

% TREC 14, 2005, Terabyte
EXPERIMENT.track.T14.id = 'T14';
EXPERIMENT.track.T14.name =  'TREC 14, 2005, Terabyte';
EXPERIMENT.track.T14.corpus = 'GOV2';
EXPERIMENT.track.T14.topics = 50;
EXPERIMENT.track.T14.runs = 58;

% TREC 15, 2006, Terabyte
EXPERIMENT.track.T15.id = 'T15';
EXPERIMENT.track.T15.name =  'TREC 15, 2006, Terabyte';
EXPERIMENT.track.T15.corpus = 'GOV2';
EXPERIMENT.track.T15.topics = 50;
EXPERIMENT.track.T15.runs = 80;

% TREC 13, 2004, Terabyte - Yubin''s 50 Shards Systems
EXPERIMENT.track.T13_50s.id = 'T13_50s';
EXPERIMENT.track.T13_50s.name =  'TREC 13, 2004, Terabyte - Yubin''s 50 Shards Systems';
EXPERIMENT.track.T13_50s.corpus = 'GOV2';
EXPERIMENT.track.T13_50s.topics = 49;
EXPERIMENT.track.T13_50s.runs = 50;
EXPERIMENT.track.T13_50s.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Terabyte/pool/qrels.2004.Terabyte.txt';
EXPERIMENT.track.T13_50s.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T13_50s.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T13_50s.pool.delimiter = 'space';
EXPERIMENT.track.T13_50s.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T13_50s.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T13_50s.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T13_50s.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T13_50s.run.path = '/Users/ferro/Documents/pubblicazioni/2017/Yubin/experiment/runs/T13_50s';
EXPERIMENT.track.T13_50s.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T13_50s.run.singlePrecision = true;
EXPERIMENT.track.T13_50s.run.delimiter = 'space';
EXPERIMENT.track.T13_50s.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T13_50s.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T13_50s.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T13_50s.run.delimiter, 'Verbose', false);

% TREC 14, 2005, Terabyte - Yubin''s 50 Shards Systems
EXPERIMENT.track.T14_50s.id = 'T14_50s';
EXPERIMENT.track.T14_50s.name =  'TREC 14, 2005, Terabyte - Yubin''s 50 Shards Systems';
EXPERIMENT.track.T14_50s.corpus = 'GOV2';
EXPERIMENT.track.T14_50s.topics = 50;
EXPERIMENT.track.T14_50s.runs = 50;
EXPERIMENT.track.T14_50s.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_14_2005_Terabyte/pool/terabyte.2005.adhoc_qrels.txt';
EXPERIMENT.track.T14_50s.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T14_50s.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T14_50s.pool.delimiter = 'space';
EXPERIMENT.track.T14_50s.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T14_50s.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T14_50s.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T14_50s.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T14_50s.run.path = '/Users/ferro/Documents/pubblicazioni/2017/Yubin/experiment/runs/T14_50s';
EXPERIMENT.track.T14_50s.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T14_50s.run.singlePrecision = true;
EXPERIMENT.track.T14_50s.run.delimiter = 'space';
EXPERIMENT.track.T14_50s.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T14_50s.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T14_50s.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T14_50s.run.delimiter, 'Verbose', false);


% TREC 15, 2006, Terabyte - Yubin''s 50 Shards Systems
EXPERIMENT.track.T15_50s.id = 'T15_50s';
EXPERIMENT.track.T15_50s.name =  'TREC 15, 2006, Terabyte - Yubin''s 50 Shards Systems';
EXPERIMENT.track.T15_50s.corpus = 'GOV2';
EXPERIMENT.track.T15_50s.topics = 50;
EXPERIMENT.track.T15_50s.runs = 50;
EXPERIMENT.track.T15_50s.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_15_2006_Terabyte/pool/qrels.tb06.top50.txt';
EXPERIMENT.track.T15_50s.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T15_50s.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T15_50s.pool.delimiter = 'space';
EXPERIMENT.track.T15_50s.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T15_50s.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T15_50s.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T15_50s.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T15_50s.run.path = '/Users/ferro/Documents/pubblicazioni/2017/Yubin/experiment/runs/T15_50s';
EXPERIMENT.track.T15_50s.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T15_50s.run.singlePrecision = true;
EXPERIMENT.track.T15_50s.run.delimiter = 'space';
EXPERIMENT.track.T15_50s.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T15_50s.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T15_50s.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T15_50s.run.delimiter, 'Verbose', false);


% TREC 13, 2004, Terabyte - Yubin''s 199 Shards Systems
EXPERIMENT.track.T13_199s.id = 'T13_199s';
EXPERIMENT.track.T13_199s.name =  'TREC 13, 2004, Terabyte - Yubin''s 199 Shards Systems';
EXPERIMENT.track.T13_199s.corpus = 'GOV2';
EXPERIMENT.track.T13_199s.topics = 49;
EXPERIMENT.track.T13_199s.runs = 50;
EXPERIMENT.track.T13_199s.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Terabyte/pool/qrels.2004.Terabyte.txt';
EXPERIMENT.track.T13_199s.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T13_199s.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T13_199s.pool.delimiter = 'space';
EXPERIMENT.track.T13_199s.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T13_199s.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T13_199s.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T13_199s.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T13_199s.run.path = '/Users/ferro/Documents/pubblicazioni/2017/Yubin/experiment/runs/T13_199s';
EXPERIMENT.track.T13_199s.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T13_199s.run.singlePrecision = true;
EXPERIMENT.track.T13_199s.run.delimiter = 'space';
EXPERIMENT.track.T13_199s.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T13_199s.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T13_199s.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T13_199s.run.delimiter, 'Verbose', false);

% TREC 14, 2005, Terabyte - Yubin''s 199 Shards Systems
EXPERIMENT.track.T14_199s.id = 'T14_199s';
EXPERIMENT.track.T14_199s.name =  'TREC 14, 2005, Terabyte - Yubin''s 199 Shards Systems';
EXPERIMENT.track.T14_199s.corpus = 'GOV2';
EXPERIMENT.track.T14_199s.topics = 50;
EXPERIMENT.track.T14_199s.runs = 50;
EXPERIMENT.track.T14_199s.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_14_2005_Terabyte/pool/terabyte.2005.adhoc_qrels.txt';
EXPERIMENT.track.T14_199s.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T14_199s.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T14_199s.pool.delimiter = 'space';
EXPERIMENT.track.T14_199s.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T14_199s.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T14_199s.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T14_199s.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T14_199s.run.path = '/Users/ferro/Documents/pubblicazioni/2017/Yubin/experiment/runs/T14_199s';
EXPERIMENT.track.T14_199s.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T14_199s.run.singlePrecision = true;
EXPERIMENT.track.T14_199s.run.delimiter = 'space';
EXPERIMENT.track.T14_199s.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T14_199s.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T14_199s.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T14_199s.run.delimiter, 'Verbose', false);


% TREC 15, 2006, Terabyte - Yubin''s 199 Shards Systems
EXPERIMENT.track.T15_199s.id = 'T15_199s';
EXPERIMENT.track.T15_199s.name =  'TREC 15, 2006, Terabyte - Yubin''s 199 Shards Systems';
EXPERIMENT.track.T15_199s.corpus = 'GOV2';
EXPERIMENT.track.T15_199s.topics = 50;
EXPERIMENT.track.T15_199s.runs = 50;
EXPERIMENT.track.T15_199s.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_15_2006_Terabyte/pool/qrels.tb06.top50.txt';
EXPERIMENT.track.T15_199s.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T15_199s.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T15_199s.pool.delimiter = 'space';
EXPERIMENT.track.T15_199s.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T15_199s.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T15_199s.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T15_199s.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T15_199s.run.path = '/Users/ferro/Documents/pubblicazioni/2017/Yubin/experiment/runs/T15_199s';
EXPERIMENT.track.T15_199s.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T15_199s.run.singlePrecision = true;
EXPERIMENT.track.T15_199s.run.delimiter = 'space';
EXPERIMENT.track.T15_199s.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T15_199s.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T15_199s.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T15_199s.run.delimiter, 'Verbose', false);


% Returns the name of a track given its index in EXPERIMENT.track.list
EXPERIMENT.track.getName = @(idx) ( EXPERIMENT.track.(EXPERIMENT.track.list{idx}).name ); 



%% Patterns for file names

% TXT - Pattern EXPERIMENT.path.shard/<splitID>/<shardID>.txt
EXPERIMENT.pattern.file.shard = @(splitID, shardID) sprintf('%1$s%2$s%3$s%2$s%4$s.txt', EXPERIMENT.path.shard, filesep, splitID, shardID);

% ALL - Pattern <path>/<trackID>/<fileID>.<ext>
EXPERIMENT.pattern.file.general.track = @(path, trackID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s.%5$s', path, trackID, filesep, fileID, ext);

% ALL - Pattern <path>/<trackID>/<splitID>/<fileID>.<ext>
EXPERIMENT.pattern.file.general.shard = @(path, trackID, splitID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s%3$s%5$s.%6$s', path, trackID, filesep, splitID, fileID, ext);

% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset.corpus = @(trackID, datasetID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.dataset, trackID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<splitID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset.shard = @(trackID, splitID, datasetID) EXPERIMENT.pattern.file.general.shard(EXPERIMENT.path.dataset, trackID, splitID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure.corpus = @(trackID, measureID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.measure, trackID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<splitID>/<measureID>.mat
EXPERIMENT.pattern.file.measure.shard = @(trackID, splitID, measureID) EXPERIMENT.pattern.file.general.shard(EXPERIMENT.path.measure, trackID, splitID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis = @(trackID, analysisID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.analysis, trackID, analysisID, 'mat');

% PDF - Pattern EXPERIMENT.path.base/figure/<trackID>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, figureID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.figure, trackID, figureID, 'pdf');

% TEX - Pattern EXPERIMENT.path.base/report/<trackID>/<reportID>
EXPERIMENT.pattern.file.report = @(trackID, reportID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.report, trackID, reportID, 'tex');


%% Patterns for identifiers

% Pattern sc<shardID>
EXPERIMENT.pattern.identifier.shard =  @(shardID) sprintf('s%05d', shardID);

% Pattern pool_<corpusID>_<trackID>
EXPERIMENT.pattern.identifier.pool.corpus =  @(corpusID, trackID) sprintf('pool_%1$s_%2$s', corpusID, trackID);

% Pattern pool_<splitID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.pool.shard =  @(splitID, shardID, trackID) sprintf('pool_%1$s_%2$s_%3$s', splitID, shardID, trackID);

% Pattern run_<corpusID>_<trackID>
EXPERIMENT.pattern.identifier.run.corpus = @(corpusID, trackID) sprintf('run_%1$s_%2$s', corpusID, trackID);

% Pattern run_<splitID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.run.shard = @(splitID, shardID, trackID) sprintf('run_%1$s_%2$s_%3$s', splitID, shardID, trackID);

% Pattern <measureID>_<corpusID>_<trackID>
EXPERIMENT.pattern.identifier.measure.corpus =  @(measureID, corpusID, trackID) sprintf('%1$s_%2$s_%3$s', measureID, corpusID, trackID);

% Pattern <measureID>_<splitID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.measure.shard =  @(measureID, splitID, shardID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', measureID,  splitID, shardID, trackID);

% Pattern <rqID>_<type>_<balanced>_sst<sstype>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.general =  @(rqID, type, balanced, sstype, quartile, measureID, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s_%8$s', rqID, type, balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_anova_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.analysis =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'anova', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_obs_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.obs =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'obs', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_tbl_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.tbl =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'tbl', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_sts_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.sts =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'sts', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_blc_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.blc =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'blc', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_vld_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.vld =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'vld', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_me_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.me =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'me', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_<factor>-me_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mePlot =  @(rqID, factor, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, [factor '-me'], balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_ie_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.ie =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'ie', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_<factor1>-<factor2>-ie_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.iePlot =  @(rqID, factor1, factor2, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, [factor1 '-' factor2 '-ie'], balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_mie_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mie =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'mie', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_mc_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mc =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'mc', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_soa_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.soa =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'soa', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <rqID>_soa_<balanced>_sst<sstype>_q<quartile>_<measureID>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.soaPlot =  @(rqID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'soa', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern shard_stats_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.stats.shard =  @(splitID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', 'shard', 'stats', splitID, trackID);


% Pattern detailed_shard_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.shard =  @(splitID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', 'detailed', 'shard', splitID, trackID);

% Pattern <rqID>_detailed_anova_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.anova.detailed =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'detailed_anova', balanced, sstype, quartile, splitID, trackID);

% Pattern <rqID>_summary_anova_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.anova.summary =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'summary_anova', balanced, sstype, quartile, splitID, trackID);

% Pattern <rqID>_overall_anova_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.anova.overall =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'overall_anova', balanced, sstype, quartile, splitID, trackID);

% Pattern <rqID>_overall_anova_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.anova.counts =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'counts_anova', balanced, sstype, quartile, splitID, trackID);


% Pattern <rqID>_detailed_tukey_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.tukey.detailed =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'detailed_tukey', balanced, sstype, quartile, splitID, trackID);

% Pattern <rqID>_summary_tukey_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.tukey.summary =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'summary_tukey', balanced, sstype, quartile, splitID, trackID);


%% Configuration for measures

% The list of measures under experimentation
%EXPERIMENT.measure.list = {'ap', 'p10', 'rprec', 'rbp', 'ndcg', 'ndcg20', 'err', 'twist', 'p5', 'p20', 'p50', 'p100', 'ndcg5', 'ndcg10', 'ndcg50', 'ndcg100'};
EXPERIMENT.measure.list = {'ap',  'rprec', 'rbp', 'ndcg',  'err', 'twist', 'p5', 'p10', 'p20', 'p50', 'p100', 'ndcg5', 'ndcg10', 'ndcg20', 'ndcg50', 'ndcg100'};
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for AP
EXPERIMENT.measure.ap.id = 'ap';
EXPERIMENT.measure.ap.acronym = 'AP';
EXPERIMENT.measure.ap.name = 'Average Precision';
EXPERIMENT.measure.ap.compute = @(pool, runSet, shortNameSuffix) averagePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix);

% Configuration for P@5
EXPERIMENT.measure.p5.id = 'p5';
EXPERIMENT.measure.p5.acronym = 'P@5';
EXPERIMENT.measure.p5.name = 'Precision at 5 Retrieved Documents';
EXPERIMENT.measure.p5.cutoffs = 5;
EXPERIMENT.measure.p5.compute = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Cutoffs', EXPERIMENT.measure.p5.cutoffs);

% Configuration for P@10
EXPERIMENT.measure.p10.id = 'p10';
EXPERIMENT.measure.p10.acronym = 'P@10';
EXPERIMENT.measure.p10.name = 'Precision at 10 Retrieved Documents';
EXPERIMENT.measure.p10.cutoffs = 10;
EXPERIMENT.measure.p10.compute = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Cutoffs', EXPERIMENT.measure.p10.cutoffs);

% Configuration for P@20
EXPERIMENT.measure.p20.id = 'p20';
EXPERIMENT.measure.p20.acronym = 'P@20';
EXPERIMENT.measure.p20.name = 'Precision at 20 Retrieved Documents';
EXPERIMENT.measure.p20.cutoffs = 20;
EXPERIMENT.measure.p20.compute = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Cutoffs', EXPERIMENT.measure.p20.cutoffs);

% Configuration for P@50
EXPERIMENT.measure.p50.id = 'p50';
EXPERIMENT.measure.p50.acronym = 'P@50';
EXPERIMENT.measure.p50.name = 'Precision at 50 Retrieved Documents';
EXPERIMENT.measure.p50.cutoffs = 50;
EXPERIMENT.measure.p50.compute = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Cutoffs', EXPERIMENT.measure.p50.cutoffs);

% Configuration for P@100
EXPERIMENT.measure.p100.id = 'p100';
EXPERIMENT.measure.p100.acronym = 'P@100';
EXPERIMENT.measure.p100.name = 'Precision at 100 Retrieved Documents';
EXPERIMENT.measure.p100.cutoffs = 100;
EXPERIMENT.measure.p100.compute = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Cutoffs', EXPERIMENT.measure.p100.cutoffs);

% Configuration for R-prec
EXPERIMENT.measure.rprec.id = 'rprec';
EXPERIMENT.measure.rprec.acronym = 'R-prec';
EXPERIMENT.measure.rprec.name = 'Precision at the Recall Base';
EXPERIMENT.measure.rprec.compute = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Rprec', true);

% Configuration for RBP
EXPERIMENT.measure.rbp.id = 'rbp';
EXPERIMENT.measure.rbp.acronym = 'RBP';
EXPERIMENT.measure.rbp.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp.persistence = 0.8;
EXPERIMENT.measure.rbp.compute = @(pool, runSet, shortNameSuffix) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.rbp.persistence);

% Configuration for nDCG
EXPERIMENT.measure.ndcg.id = 'ndcg';
EXPERIMENT.measure.ndcg.acronym = 'nDCG';
EXPERIMENT.measure.ndcg.name = 'Normalized Discounted Cumulated Gain at Last Retrieved Document';
EXPERIMENT.measure.ndcg.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.ndcg.logBase = 10;
EXPERIMENT.measure.ndcg.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg.compute = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.ndcg.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for nDCG@5
EXPERIMENT.measure.ndcg5.id = 'ndcg5';
EXPERIMENT.measure.ndcg5.acronym = 'nDCG@5';
EXPERIMENT.measure.ndcg5.name = 'Normalized Discounted Cumulated Gain at 5 Retrieved Documents';
EXPERIMENT.measure.ndcg5.cutoffs = 5;
EXPERIMENT.measure.ndcg5.logBase = 10;
EXPERIMENT.measure.ndcg5.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg5.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg5.compute = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.ndcg5.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg5.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg5.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg5.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for nDCG@10
EXPERIMENT.measure.ndcg10.id = 'ndcg10';
EXPERIMENT.measure.ndcg10.acronym = 'nDCG@10';
EXPERIMENT.measure.ndcg10.name = 'Normalized Discounted Cumulated Gain at 10 Retrieved Documents';
EXPERIMENT.measure.ndcg10.cutoffs = 10;
EXPERIMENT.measure.ndcg10.logBase = 10;
EXPERIMENT.measure.ndcg10.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg10.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg10.compute = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.ndcg10.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg10.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg10.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg10.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for nDCG@20
EXPERIMENT.measure.ndcg20.id = 'ndcg20';
EXPERIMENT.measure.ndcg20.acronym = 'nDCG@20';
EXPERIMENT.measure.ndcg20.name = 'Normalized Discounted Cumulated Gain at 20 Retrieved Documents';
EXPERIMENT.measure.ndcg20.cutoffs = 20;
EXPERIMENT.measure.ndcg20.logBase = 10;
EXPERIMENT.measure.ndcg20.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg20.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg20.compute = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.ndcg20.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg20.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg20.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg20.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for nDCG@50
EXPERIMENT.measure.ndcg50.id = 'ndcg50';
EXPERIMENT.measure.ndcg50.acronym = 'nDCG@50';
EXPERIMENT.measure.ndcg50.name = 'Normalized Discounted Cumulated Gain at 50 Retrieved Documents';
EXPERIMENT.measure.ndcg50.cutoffs = 50;
EXPERIMENT.measure.ndcg50.logBase = 10;
EXPERIMENT.measure.ndcg50.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg50.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg50.compute = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.ndcg50.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg50.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg50.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg50.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for nDCG@100
EXPERIMENT.measure.ndcg100.id = 'ndcg100';
EXPERIMENT.measure.ndcg100.acronym = 'nDCG@100';
EXPERIMENT.measure.ndcg100.name = 'Normalized Discounted Cumulated Gain at 100 Retrieved Documents';
EXPERIMENT.measure.ndcg100.cutoffs = 100;
EXPERIMENT.measure.ndcg100.logBase = 10;
EXPERIMENT.measure.ndcg100.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg100.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg100.compute = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.ndcg100.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg100.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg100.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg100.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for ERR
EXPERIMENT.measure.err.id = 'err';
EXPERIMENT.measure.err.acronym = 'ERR';
EXPERIMENT.measure.err.name = 'Expected Reciprocal Rank at Last Retrieved Document';
EXPERIMENT.measure.err.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.err.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.err.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.err.compute = @(pool, runSet, shortNameSuffix) expectedReciprocalRank(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.err.cutoffs, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.err.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.err.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for Twist
EXPERIMENT.measure.twist.id = 'twist';
EXPERIMENT.measure.twist.acronym = 'Twist';
EXPERIMENT.measure.twist.name = 'Twist';
EXPERIMENT.measure.twist.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.twist.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.twist.compute = @(pool, runSet, shortNameSuffix) twist(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.twist.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.twist.fixedNumberRetrievedDocumentsPaddingStrategy);

% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 


%% Configuration for analyses

% The significance level for the analyses
EXPERIMENT.analysis.alpha.threshold = 0.05;
EXPERIMENT.analysis.alpha.color = 'lightgrey';

EXPERIMENT.analysis.smallEffect.threshold = 0.06;
EXPERIMENT.analysis.smallEffect.color = 'verylightblue';

EXPERIMENT.analysis.mediumEffect.threshold = 0.14;
EXPERIMENT.analysis.mediumEffect.color = 'lightblue';

EXPERIMENT.analysis.largeEffect.color = 'blue';

EXPERIMENT.analysis.label.subject = 'Topic';
EXPERIMENT.analysis.color.subject = rgb('FireBrick');

EXPERIMENT.analysis.label.factorA = 'System';
EXPERIMENT.analysis.color.factorA = rgb('RoyalBlue');

EXPERIMENT.analysis.label.factorB = 'Shard';
EXPERIMENT.analysis.color.factorB = rgb('ForestGreen');

% The list of the possible balancing types
% - if |unb| it will use an unbalanced design where missing values 
%   are denoted by |NaN|;
% - if |zero|, it will force a balanced design by substituting 
%   |NaN| values with zeros; 
% - if |one|, it will force a balanced design by substituting |NaN| values
%   with ones; 
% - if |med|, it will force a balanced design by substituting |NaN| values 
%   with the median value (ignoring |NaN|s) across all topics and systems; 
% - if |mean|, it will force a balanced design by substituting  |NaN| 
%   values with the mean value (ignoring |NaN|s) across all topics and 
%   systems; 
% - if |lq|, it will force a balanced design by substituting |NaN| values 
%   with the lower quartile value (ignoring |NaN|s) across all topics and 
%   systems;
% - if |uq|, it will force a balanced design by substituting |NaN| values 
%   with the upper quartile value (ignoring |NaN|s) across all topics and 
%   systems;
% - if |top|, keep only the topics with at least one relevant document for
% each shard;
% - if |b|, assume an already balanced design;
EXPERIMENT.analysis.balanced.list = {'unb', 'zero', 'one', 'mean', 'med', 'lq', 'uq', 'top', 'b'};

EXPERIMENT.analysis.balanced.unb.id = 'unb';
EXPERIMENT.analysis.balanced.unb.description  = 'Unbalanced design where missing values are denoted by NaN';

EXPERIMENT.analysis.balanced.zero.id = 'zero';
EXPERIMENT.analysis.balanced.zero.description  = 'Balanced design where missing values are forced to zero';

EXPERIMENT.analysis.balanced.one.id = 'one';
EXPERIMENT.analysis.balanced.one.description  = 'Balanced design where missing values are forced to one';

EXPERIMENT.analysis.balanced.mean.id = 'mean';
EXPERIMENT.analysis.balanced.mean.description  = 'Balanced design where missing values are forced to the mean value';

EXPERIMENT.analysis.balanced.med.id = 'med';
EXPERIMENT.analysis.balanced.med.description  = 'Balanced design where missing values are forced to the median value';

EXPERIMENT.analysis.balanced.lq.id = 'lq';
EXPERIMENT.analysis.balanced.lq.description  = 'Balanced design where missing values are forced to the lower quartile value';

EXPERIMENT.analysis.balanced.uq.id = 'uq';
EXPERIMENT.analysis.balanced.uq.description  = 'Balanced design where missing values are forced to the upper quartile value';

% The possible quartiles used in the experiments
EXPERIMENT.analysis.quartile.list = {'q1', 'q2', 'q3', 'q4'};
EXPERIMENT.analysis.quartile.q1.id = 'q1';
EXPERIMENT.analysis.quartile.q1.description  = 'Systems up to first quartile (top 25\%) of performance';

EXPERIMENT.analysis.quartile.q2.id = 'q2';
EXPERIMENT.analysis.quartile.q2.description  = 'Systems up to second quartile (top 50\%)/median of performance';

EXPERIMENT.analysis.quartile.q3.id = 'q3';
EXPERIMENT.analysis.quartile.q3.description  = 'Systems up to third quartile (top 75\%) of performance';

EXPERIMENT.analysis.quartile.q4.id = 'q4';
EXPERIMENT.analysis.quartile.q4.description  = 'All systems used';


% The list of the possible ANOVA analyses
EXPERIMENT.analysis.list = {'rq0', 'rq1', 'rq2', 'rq3', 'rq3SIGIR17', 'rq4', 'rq5', 'rq5SIGIR17'};

% Topic/System Effects on Whole Corpus
EXPERIMENT.analysis.rq0.id = 'rq0';
EXPERIMENT.analysis.rq0.name = 'Topic/System Effects on Whole Corpus';
EXPERIMENT.analysis.rq0.description = 'Crossed effects GLMM on whole corpus: subjects are topics; effects are systems';
EXPERIMENT.analysis.rq0.glmm = '$Y_{ij} = \mu_{\cdot\cdot} + \tau_i + \alpha_j + \varepsilon_{ij}$';
% the model = Topic (subject) + System (factorA)
EXPERIMENT.analysis.rq0.model = [1 0; ... 
                                 0 1];
EXPERIMENT.analysis.rq0.compute = @(data, subject, factorA, sstype) anovan(data, {subject, factorA}, ...
        'Model', EXPERIMENT.analysis.rq0.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    

    
% Topic/System Effects on Shards
EXPERIMENT.analysis.rq1.id = 'rq1';
EXPERIMENT.analysis.rq1.name = 'Topic/System Effects on Shards';
EXPERIMENT.analysis.rq1.description = 'Crossed effects GLMM on shards: subjects are topics; effects are systems';
EXPERIMENT.analysis.rq1.glmm = '$Y_{ij} = \mu_{\cdot\cdot} + \tau_i + \alpha_j + \varepsilon_{ij}$';
% the model = Topic (subject) + System (factorA) 
EXPERIMENT.analysis.rq1.model = [1 0; ... 
                                 0 1];
EXPERIMENT.analysis.rq1.compute = @(data, subject, factorA, sstype) anovan(data, {subject, factorA}, ...
        'Model', EXPERIMENT.analysis.rq1.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    

% Topic/System/Shard Effects on Shards
EXPERIMENT.analysis.rq2.id = 'rq2';
EXPERIMENT.analysis.rq2.name = 'Topic/System/Shard Effects on Shards';
EXPERIMENT.analysis.rq2.description = 'Crossed effects GLMM on shards: subjects are topics; effects are systems and shards';
EXPERIMENT.analysis.rq2.glmm = '$Y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \beta_k +  \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) + Corpus (factorB) +
%             System * Shard
EXPERIMENT.analysis.rq2.model = [1 0 0; ... 
                                 0 1 0; ...
                                 0 0 1];
EXPERIMENT.analysis.rq2.compute = @(data, subject, factorA, factorB, sstype) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.rq2.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');
    
    
    
% Topic/System/Shard Effects and System*Shard Interaction on Shards
EXPERIMENT.analysis.rq3.id = 'rq3';
EXPERIMENT.analysis.rq3.name = 'Topic/System/Shard Effects and System*Shard Interaction on Shards';
EXPERIMENT.analysis.rq3.description = 'Crossed effects GLMM on shards: subjects are topics; effects are systems and shards; system*shard interaction';
EXPERIMENT.analysis.rq3.glmm = '$Y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \beta_k + (\alpha\beta)_{jk} + \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) + Corpus (factorB) +
%             System*Shard
EXPERIMENT.analysis.rq3.model = [1 0 0; ... 
                                 0 1 0; ...
                                 0 0 1; ...
                                 0 1 1];
EXPERIMENT.analysis.rq3.compute = @(data, subject, factorA, factorB, sstype) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.rq3.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');
    
% RQ3 variant according to the SIGIR 2017 paper by Ferro and Sanderson
EXPERIMENT.analysis.rq3SIGIR17.id = 'rq3SIGIR17';
EXPERIMENT.analysis.rq3SIGIR17.name = EXPERIMENT.analysis.rq3.name;
EXPERIMENT.analysis.rq3SIGIR17.description = EXPERIMENT.analysis.rq3.description;
EXPERIMENT.analysis.rq3SIGIR17.glmm = EXPERIMENT.analysis.rq3.glmm;
EXPERIMENT.analysis.rq3SIGIR17.model = EXPERIMENT.analysis.rq3.model;
EXPERIMENT.analysis.rq3SIGIR17.compute = EXPERIMENT.analysis.rq3.compute;
           
% Topic/System/Corpus Effects on Shards
EXPERIMENT.analysis.rq4.id = 'rq4';
EXPERIMENT.analysis.rq4.name = 'Topic/System/Shard Effects and System*Shard/Topic*System Interactions on Shards';
EXPERIMENT.analysis.rq4.description = 'Crossed effects GLMM on shards: subjects are topics; effects are systems and shards; system*shard and topic*system interactions';
EXPERIMENT.analysis.rq4.glmm = '$Y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \beta_k + (\alpha\beta)_{jk} + (\tau\alpha)_{ij} + \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) + Corpus (factorB) +
%             System*Shard + Topic*System
EXPERIMENT.analysis.rq4.model = [1 0 0; ... 
                                 0 1 0; ...
                                 0 0 1; ...
                                 0 1 1; ...
                                 1 1 0];
EXPERIMENT.analysis.rq4.compute = @(data, subject, factorA, factorB, sstype) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.rq4.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    

% Topic/System/Corpus Effects on Shards
EXPERIMENT.analysis.rq5.id = 'rq5';
EXPERIMENT.analysis.rq5.name = 'Topic/System/Shard Effects and System*Shard/Topic*System Interactions/Topic*Shard Interactions on Shards';
EXPERIMENT.analysis.rq5.description = 'Crossed effects GLMM on shards: subjects are topics; effects are systems and shards; system*shard, topic*system, and topic*shard interactions';
EXPERIMENT.analysis.rq5.glmm = '$Y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \beta_k + (\alpha\beta)_{jk} (\tau\alpha)_{ij} + (\tau\beta)_{ik} + \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) + Corpus (factorB) +
%             System*Shard + Topic*System + Topic*Shard
EXPERIMENT.analysis.rq5.model = [1 0 0; ... 
                                 0 1 0; ...
                                 0 0 1; ...
                                 0 1 1; ...
                                 1 1 0; ...
                                 1 0 1];
EXPERIMENT.analysis.rq5.compute = @(data, subject, factorA, factorB, sstype) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.rq5.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    

% RQ5 variant according to the SIGIR 2017 paper by Ferro and Sanderson
EXPERIMENT.analysis.rq5SIGIR17.id = 'rq5SIGIR17';
EXPERIMENT.analysis.rq5SIGIR17.name = EXPERIMENT.analysis.rq5.name;
EXPERIMENT.analysis.rq5SIGIR17.description = EXPERIMENT.analysis.rq5.description;
EXPERIMENT.analysis.rq5SIGIR17.glmm = EXPERIMENT.analysis.rq5.glmm;
EXPERIMENT.analysis.rq5SIGIR17.model = EXPERIMENT.analysis.rq5.model;
EXPERIMENT.analysis.rq5SIGIR17.compute = EXPERIMENT.analysis.rq5.compute;
        
% Tukey HSD multiple comparison analysis for the system factor
EXPERIMENT.analysis.multcompare.system = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2], 'Display', 'off');

% Jarque-Bera Test for normality of the residuals
EXPERIMENT.analysis.validation.normality = @(sts) jbtest(sts.resid, EXPERIMENT.analysis.alpha.threshold);

% Levene Test for homoscedasticity of the residuals
EXPERIMENT.analysis.validation.homoscedasticity = @(sts, factor) vartestn(sts.resid, factor, 'TestType','LeveneAbsolute', 'Display', 'off');

