Matlab source code for running the experiments reported in the paper:

* Ferro, N., Kim, Y., and Sanderson, M. (2018). Shard Effects on Effectiveness. Submitted to _ACM Transactions on Information Systems (TOIS)_.
